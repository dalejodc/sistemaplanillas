import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { TipoCorreo } from '../models/tipo_correo';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class TipoCorreoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json'
		//'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getTiposCorreos(){
		return this._http.get(this.url+'tipocorreo').map(res=> res.json());
	}

	getCorreo(id){
		return this._http.get(this.url+'tipocorreo/'+id, {headers: this.headers}).map(res=> res.json());
	}


	addCorreo(tipoCorreo : TipoCorreo){
		let newTipoCorreo = JSON.stringify(tipoCorreo); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'tipocorreo', newTipoCorreo, {headers:this.headers})
						 .map(res => res.json());
	}

	editCorreo(id, tipoCorreo: TipoCorreo){
		let newTipoCorreo = JSON.stringify(tipoCorreo); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'tipocorreo/'+ id, newTipoCorreo, {headers: this.headers})
						  .map(res => res.json());
	}
}