import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { TipoTransaccion } from '../models/tipo_transaccion';
import { GlobalConfig } from  '../services/global-config';

@Injectable()
export class TipoTransaccionService{

	public url : string;
	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		this.url = GlobalConfig.url + 'tipotransaccion';
		
	}

	getTipoTransacciones(){
		return this._http.get(this.url).map(res=> res.json());
	}

	deleteTipoTransaccion(id){

		return this._http.delete(this.url + '/' + id,{headers: this.headers}).map(res => res.json());
	}


	addTipoTransaccion(transaccion:TipoTransaccion){
		
		let json = JSON.stringify(transaccion);
		let params = json;
		

		return this._http.post(this.url, params, {headers:this.headers}).map(res => res.json());
	}

	updateTipoTransaccion(id , transaccion:TipoTransaccion){
		
		let json = JSON.stringify(transaccion);
		let params = json;
		

	return this._http.put(this.url + '/' + id, params, {headers: this.headers}).map(res => res.json());
	}

	getTipoTransaccion(id){
		return this._http.get(this.url + '/' + id).map(res => res.json());

	}
}