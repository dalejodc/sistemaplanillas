import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { EstadoCivil } from '../models/estado_civil';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class EstadoCivilService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getEstadoCiviles(){
		return this._http.get(this.url+'estadocivil', {headers: this.headers}).map(res=> res.json());
	}

	getEstadoCivil(id){
		return this._http.get(this.url+'estadocivil/'+id, {headers: this.headers}).map(res=> res.json());
	}

	// getGenerosDeshabilitados(){
	// 	return this._http.get(this.url+'estadocivil/disabled/all').map(res=> res.json());
	// }

	addEstadoCivil(estadoCivil : EstadoCivil){
		let newestadocivil = JSON.stringify(estadoCivil); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'estadocivil', newestadocivil, {headers:this.headers})
						 .map(res => res.json());
	}

	editEstadoCivil(id, estadoCivil: EstadoCivil){
		let jsongenero = JSON.stringify(estadoCivil); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'estadocivil/'+ id, jsongenero, {headers: this.headers})
						  .map(res => res.json());
	}

	deleteEstadoCivil(id){
		return this._http.delete(this.url+'estadocivil/'+ id, {headers: this.headers})
						  .map(res => res.json());
	}
}