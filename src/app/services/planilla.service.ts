import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Planilla } from '../models/planilla';
import { Venta } from '../models/venta'
import {PeriodoPlanilla} from '../models/periodoPlanilla'
import { GlobalConfig } from  '../services/global-config'


@Injectable()
export class PlanillaService{

	public url : string;
	public url2 : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url + 'planilla/';
		this.url2 = GlobalConfig.url + 'periodoplanilla/';
	}
	// getModels(){
	// 		return this._http.get(this.url).map(res=> res.json());
	// 	}

		getPlanilla(id){
			return this._http.get(this.url+id, {headers: this.headers}).map(res=> res.json());
		}

		getPeriodoPlanilla(){
			return this._http.get(this.url2, {headers: this.headers}).map(res=> res.json());

		}

		getPlanillasByPeriodoId(id){
			return this._http.get(this.url2 + id, {headers: this.headers}).map(res=> res.json());

		}

		closePeriodo(){
			return this._http.get(this.url2 + '/close', {headers: this.headers}).map(res=> res.json());

		}

		guardarPeriodo(periodo_planilla_fecha_inicio: Date){
			let params=JSON.stringify(new PeriodoPlanilla(null,null,periodo_planilla_fecha_inicio,null,null));
			console.log(params);
			return this._http.post(this.url2,params,{headers: this.headers}).map(res=>res.json);
		}

		regVenta(venta: Venta){

			let params = JSON.stringify(venta);

			return this._http.post(this.url + 'ingresarcomision', params, {headers: this.headers}).map(res => res.json);

		}

		// addModel(model : Model){
		// 	let newModel = JSON.stringify(model); //Convirtiendo el objeto a JSON

		// 	return this._http.post(this.url, newModel, {headers:this.headers})
		// 					 .map(res => res.json());
		// }

		// updateModel(id, model: Model){
		// 	let jsonModel = JSON.stringify(model); //Convirtiendo el objeto a JSON

		// 	return this._http.put(this.url+ id, jsonModel, {headers: this.headers})
		// 					  .map(res => res.json());
		// }


		// deleteModel(id){
		// 	return this._http.delete(this.url +  id,{headers: this.headers}).map(res => res.json());
		// }



}
