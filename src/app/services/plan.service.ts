import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { GlobalConfig } from  '../services/global-config'
import { Plan } from '../models/plan'

@Injectable()
export class PlanService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json'
		//'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getPlanes(){
		return this._http.get(this.url+'plan').map(res=> res.json());
	}

	getPlan(id){
		return this._http.get(this.url+'plan/'+id, {headers: this.headers}).map(res=> res.json());
	}

	// getGenerosDeshabilitados(){
	// 	return this._http.get(this.url+'genero/disabled/all').map(res=> res.json());
	// }

	addPlan(plan : Plan){
		let newplan = JSON.stringify(plan); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'plan', newplan, {headers:this.headers})
						 .map(res => res.json());
	}

	editPlan(id, plan: Plan){
		let jsonplan = JSON.stringify(plan); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'plan/'+ id, jsonplan, {headers: this.headers})
						  .map(res => res.json());
	}
}