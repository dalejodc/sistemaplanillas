import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Comision } from '../models/comision';
import { GlobalConfig } from  '../services/global-config'


@Injectable()
export class ComisionService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url + 'comision/';
	}
	getComisiones(){
			return this._http.get(this.url).map(res=> res.json());
		}
	
	getComision(id){
			return this._http.get(this.url+id, {headers: this.headers}).map(res=> res.json());
	}
	
	addComision(comision : Comision){
			let newComision = JSON.stringify(comision); //Convirtiendo el objeto a JSON
	
			return this._http.post(this.url , newComision, {headers:this.headers})
							 .map(res => res.json());
	}
	
	updateComision(id, comision: Comision){
			let newComision = JSON.stringify(comision); //Convirtiendo el objeto a JSON
	
			return this._http.put(this.url+ id, newComision, {headers: this.headers})
							  .map(res => res.json());
		}
		
	
	deleteComision(id){
			return this._http.delete(this.url +  id,{headers: this.headers}).map(res => res.json());
		}
	

	

}