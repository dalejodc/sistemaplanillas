import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { TipoTelefono } from '../models/tipo_telefono';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class TipoTelefonoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json'
		//'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getTiposTelefonos(){
		return this._http.get(this.url+'tipotelefono').map(res=> res.json());
	}

	getTelefono(id){
		return this._http.get(this.url+'tipotelefono/'+id, {headers: this.headers}).map(res=> res.json());
	}


	addTelefono(tipoTelefono : TipoTelefono){
		let newTipoTelefono = JSON.stringify(tipoTelefono); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'tipotelefono', newTipoTelefono, {headers:this.headers})
						 .map(res => res.json());
	}

	editTelefono(id, tipoTelefono: TipoTelefono){
		let newTipoTelefono = JSON.stringify(tipoTelefono); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'tipotelefono/'+ id, newTipoTelefono, {headers: this.headers})
						  .map(res => res.json());
	}
}