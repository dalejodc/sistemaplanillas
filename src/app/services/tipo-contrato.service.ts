import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { TipoContrato } from '../models/tipo_contrato';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class TipoContratoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json'
		//'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getTiposContratos(){
		return this._http.get(this.url+'formacontrato').map(res=> res.json());
	}

	getTipoContrato(id){
		return this._http.get(this.url+'formacontrato/'+id, {headers: this.headers}).map(res=> res.json());
	}

	addTipoContrato(tipoContrato : TipoContrato){
		let newtipoContrato = JSON.stringify(tipoContrato); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'formacontrato', newtipoContrato, {headers:this.headers})
						 .map(res => res.json());
	}

	editTipoContrato(id, tipoContrato : TipoContrato){
		let newtipoContrato = JSON.stringify(tipoContrato); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'formacontrato/'+ id, newtipoContrato, {headers: this.headers})
						  .map(res => res.json());
	}

	deleteTipoContrato(id){
		return this._http.delete(this.url+'formacontrato/'+ id, {headers: this.headers})
						  .map(res => res.json());
	}
}