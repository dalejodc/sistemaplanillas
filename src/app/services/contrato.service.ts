import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Genero } from '../models/genero';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class ContratoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getContratos(){
		return this._http.get(this.url+'contrato',{headers: this.headers} ).map(res=> res.json());
	}

	getContrato(id){
		return this._http.get(this.url+'contrato/'+id, {headers: this.headers}).map(res=> res.json());
	}

	addContrato(contrato){
		let newContrato = JSON.stringify(contrato); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'contrato', newContrato, {headers:this.headers})
						 .map(res => res.json());
	}

	editContrato(id, contrato){
		let jsonContrato = JSON.stringify(contrato); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'genero/'+ id, jsonContrato, {headers: this.headers})
						  .map(res => res.json());
	}
}