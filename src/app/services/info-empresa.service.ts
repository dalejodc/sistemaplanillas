import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Genero } from '../models/genero';
import { GlobalConfig } from  '../services/global-config'

import { InfoEmpresa } from '../models/info_empresa';

@Injectable()
export class InfoEmpresaService{

	public url : string;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authotization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		this.url = GlobalConfig.url;
	}

	getInfoEmpresa(){
		return this._http.get(this.url+'empresa', {headers: this.headers}).map(res=> res.json());
	}

	getEmpresa(){
		return this._http.get(this.url+'empresa/'+sessionStorage.getItem('empresa'), {headers: this.headers}).map(res=> res.json());
	}

	editEmpresa(id, empresa: InfoEmpresa){
		let jsonempresa = JSON.stringify(empresa); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'empresa/'+ id, jsonempresa, {headers: this.headers})
						  .map(res => res.json());
	}
}