import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Puesto } from '../models/puesto';
import { GlobalConfig } from  '../services/global-config';

@Injectable()
export class PuestoService{

	public url : string;
	//public url = "https://phalcon-planilla.herokuapp.com/public/puesto";

		private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url+ 'puesto/';
	}

getPuestos(){
		//return this._http.get(this.url+'').map(res=> res.json());
		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
	}

getPuesto(id){
		//return this._http.get(this.url + '/' + id).map(res => res.json());
		return this._http.get(this.url + id, {headers: this.headers}).map(res=> res.json());

	}


deletePuesto(id , puesto:Puesto){
		
		/*let json = JSON.stringify(puesto);
		let params = json;
		let headers = new Headers({'Content-type':'application/json'});
		

		return this._http.put(this.url + '/' + id, params, {headers: headers}).map(res => res.json());*/
		return this._http.delete(this.url +  id,{headers: this.headers}).map(res => res.json());
	}


addPuesto(puesto:Puesto){
		
		/*let json = JSON.stringify(puesto);
		let params = json;
		let headers = new Headers({'Content-type':'application/json'});
		

		return this._http.post(this.url, params, {headers: headers}).map(res => res.json());*/
		let newpuesto = JSON.stringify(puesto); //Convirtiendo el objeto a JSON

		return this._http.post(this.url, newpuesto, {headers:this.headers})
						 .map(res => res.json());
	}

updatePuesto(id , puesto:Puesto){
		
		/*let json = JSON.stringify(puesto);
		let params = json;
		let headers = new Headers({'Content-type':'application/json'});
		

		return this._http.put(this.url + '/' + id, params, {headers: headers}).map(res => res.json());*/
		let jsonconfigP = JSON.stringify(puesto); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+ id, jsonconfigP, {headers: this.headers})
						  .map(res => res.json());
	}

}