import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Genero } from '../models/genero';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class GeneroService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getGeneros(){
		return this._http.get(this.url+'genero', {headers: this.headers}).map(res=> res.json());
	}

	getGenerosEmpresa(){
		return this._http.get(this.url+'genero/empresa',{headers:this.headers}).map(res=>res.json());
	}

	getGenero(id){
		return this._http.get(this.url+'genero/'+id, {headers: this.headers}).map(res=> res.json());
	}


	getGenerosDeshabilitados(){
		return this._http.get(this.url+'genero/disabled/all').map(res=> res.json());
	}

	addGenero(genero : Genero){
		let newgenero = JSON.stringify(genero); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'genero', newgenero, {headers:this.headers})
						 .map(res => res.json());
	}

	editGenero(id, genero: Genero){
		let jsongenero = JSON.stringify(genero); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'genero/'+ id, jsongenero, {headers: this.headers})
						  .map(res => res.json());
	}
}
