import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { TipoDocumento } from '../models/tipo_documento';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class TipoDocumentoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getTiposDocumentos(){
		return this._http.get(this.url+'tipodocumento').map(res=> res.json());
	}

	getDocumento(id){
		return this._http.get(this.url+'tipodocumento/'+id, {headers: this.headers}).map(res=> res.json());
	}


	addCorreo(tipoDocumento : TipoDocumento	){
		let newTipoDoc = JSON.stringify(tipoDocumento); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'tipodocumento', newTipoDoc, {headers:this.headers})
						 .map(res => res.json());
	}

	editCorreo(id, tipoDocumento: TipoDocumento){
		let newTipoDoc = JSON.stringify(tipoDocumento); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'tipodocumento/'+ id, newTipoDoc, {headers: this.headers})
						  .map(res => res.json());
	}
}