import  { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

@Injectable () 

export class AuthGuardService implements CanActivate { 

	constructor(){
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
 		if(sessionStorage.getItem('token')!=null){
 			return true; 
 		}else{
 			return false; 
 		}
 }
}