
import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Empresa } from '../models/empresa';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class EmpresaService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authotization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getEmpresas(){
		return this._http.get(this.url+'empresa').map(res=> res.json());
	}

	getEmpresa(id){
		return this._http.get(this.url+'empresa/'+id, {headers: this.headers}).map(res=> res.json());
	}

	enviarCorreo(id, emp){
		// let newempresa = JSON.stringify(empresa);
		return this._http.post(this.url+'empresa/solicitud/'+id, emp, {headers: this.headers}).map(res=> res.json());
	}

	getEmpresasDeshabilitadas(){
		return this._http.get(this.url+'empresa/disabled', {headers: this.headers}).map(res=> res.json());
	}

	getEmpresasPendientes(){
		return this._http.get(this.url+'empresa/pending', {headers: this.headers}).map(res=> res.json());
	}

	addEmpresa(empresa : Empresa){
		let newempresa = JSON.stringify(empresa); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'empresa', newempresa, {headers:this.headers})
						 .map(res => res.json());
	}

	editEmpresa(id, empresa: Empresa){
		let jsonempresa = JSON.stringify(empresa); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'empresa/'+ id, jsonempresa, {headers: this.headers})
						  .map(res => res.json());
	}
}