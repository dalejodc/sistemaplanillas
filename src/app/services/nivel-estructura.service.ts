import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { NivelEstructura } from '../models/nivel_estructura';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class NivelEstructuraService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getNivelesEstructura(){
		//return this._http.get(this.url+'').map(res=> res.json());
		return this._http.get(this.url+ 'nivelestructura/', {headers: this.headers}).map(res=> res.json());
	}

	getNivelesDepartamentos(){
		return this._http.get(this.url+'nivelestructura/departamentos', {headers:this.headers}).map(res=> res.json());
	}

	getNivelesPuestosByDepartamento(id){
		return this._http.get(this.url+'nivelestructura/puestos/'+id, {headers:this.headers}).map(res=> res.json());
	}
	getPaises(){
		return this._http.get(this.url+'estructuraterritorial').map(res=> res.json());
	}
	getNiveleEstructura(id){
		//return this._http.get(this.url + '/' + id).map(res => res.json());
		return this._http.get(this.url + 'nivelestructura/'+ id, {headers: this.headers}).map(res=> res.json());

	}

	// getGenero(id){
	// 	return this._http.get(this.url+'genero/'+id, {headers: this.headers}).map(res=> res.json());
	// }

	// getGenerosDeshabilitados(){
	// 	return this._http.get(this.url+'genero/disabled/all').map(res=> res.json());
	// }

	addEstructura(x){
		let est = JSON.stringify(x); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'nivelestructura', est, {headers:this.headers})
						 .map(res => res.json());
	}

	// editGenero(id, genero: Genero){
	// 	let jsongenero = JSON.stringify(genero); //Convirtiendo el objeto a JSON

	// 	return this._http.put(this.url+'genero/'+ id, jsongenero, {headers: this.headers})
	// 					  .map(res => res.json());
	// }
}