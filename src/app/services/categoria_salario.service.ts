import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { CategoriaSalario } from '../models/categoria_salario';

import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class CategoriaSalarioService{

	public url : string;
	//private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url+ 'categoriasalario/';
	}

	getCategoriaSalarios(){
		//return this._http.get(this.url+'configuracionplanilla').map(res=> res.json());

		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
	}

	updateCategoriaSalario(id, categoriasalario:CategoriaSalario){
		let jsonconfigP = JSON.stringify(categoriasalario); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+ id, jsonconfigP, {headers: this.headers})
						  .map(res => res.json());
	}

	getCategoriaSalario(id){
		//return this._http.get(this.url + '/' + id).map(res => res.json());

		return this._http.get(this.url + id, {headers: this.headers}).map(res=> res.json());

	}

	addCategoriaSalario(categoria:CategoriaSalario){
		let newcategoria = JSON.stringify(categoria); //Convirtiendo el objeto a JSON

		return this._http.post(this.url, newcategoria, {headers:this.headers})
						 .map(res => res.json());
	}

	deleteCategoriaSalario(id){
		
		return this._http.delete(this.url +  id,{headers: this.headers}).map(res => res.json());
		
	}
}
