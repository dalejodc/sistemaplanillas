import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Renta } from '../models/renta';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class RentaService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json'
		//'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getRentas(){
		return this._http.get(this.url+'renta').map(res=> res.json());
	}

	getRenta(id){
		return this._http.get(this.url+'renta/'+id, {headers: this.headers}).map(res=> res.json());
	}


	addRenta(renta : Renta){
		let newrenta = JSON.stringify(renta); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'renta', newrenta, {headers:this.headers})
						 .map(res => res.json());
	}

	editRenta(id, renta: Renta){
		let jsongenero = JSON.stringify(renta); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'renta/'+ id, jsongenero, {headers: this.headers})
						  .map(res => res.json());
	}
}