import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { TipoSalario } from '../models/tipo_salario';

import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class TipoSalarioService{

	public url : string;
	//private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url+ 'tiposalario/';
	}

	getTipoSalarios(){
		//return this._http.get(this.url+'configuracionplanilla').map(res=> res.json());

		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
	}

	updateTipoSalario(id, tiposalario:TipoSalario){
		let jsonconfigP = JSON.stringify(tiposalario); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+ id, jsonconfigP, {headers: this.headers})
						  .map(res => res.json());
	}

	getTipoSalario(id){
		//return this._http.get(this.url + '/' + id).map(res => res.json());

		return this._http.get(this.url + id, {headers: this.headers}).map(res=> res.json());

	}

	addTipoSalario(tipo:TipoSalario){
		let newtipo = JSON.stringify(tipo); //Convirtiendo el objeto a JSON

		return this._http.post(this.url, newtipo, {headers:this.headers})
						 .map(res => res.json());
	}

	deleteTipoSalario(id){
		
		return this._http.delete(this.url +  id,{headers: this.headers}).map(res => res.json());
		
	}
}