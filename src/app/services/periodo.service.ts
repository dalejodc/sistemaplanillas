import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class PeriodoService{

	public url : string;
	//private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url+'periodo/';
	}

	getPeriodos(){
		//return this._http.get(this.url+'periodo').map(res=> res.json());
		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
	}
	getPeriodo(id){
		
		return this._http.get(this.url + id, {headers: this.headers}).map(res=> res.json());
	}

}