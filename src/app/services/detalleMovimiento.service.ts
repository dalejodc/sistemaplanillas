import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { DetalleMovimiento } from '../models/detalleMovimiento';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class DetalleMovimientoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json'
		//'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url + 'detallemovimiento/';
	}
	// getModels(){
	// 		return this._http.get(this.url).map(res=> res.json());
	// 	}
	
	// 	getModel(id){
	// 		return this._http.get(this.url+id, {headers: this.headers}).map(res=> res.json());
	// 	}
	
		addDetalleMovimiento(detalleMovimiento : DetalleMovimiento){
			let newDetalleMovimiento = JSON.stringify(detalleMovimiento); //Convirtiendo el objeto a JSON
	
			return this._http.post(this.url, newDetalleMovimiento, {headers:this.headers})
							 .map(res => res.json());
		}
	
		// updateModel(id, model: Model){
		// 	let jsonModel = JSON.stringify(model); //Convirtiendo el objeto a JSON
	
		// 	return this._http.put(this.url+ id, jsonModel, {headers: this.headers})
		// 					  .map(res => res.json());
		// }
		
	
		deleteDetalleMovimiento(id){
			return this._http.delete(this.url +  id,{headers: this.headers}).map(res => res.json());
		}
	
	

}