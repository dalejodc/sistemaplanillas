
import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Empresa } from '../models/empresa';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class UsuarioService{

	public url : string;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authotization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		this.url = GlobalConfig.url;
	}

	getUsuario(){
		return this._http.get(this.url+'usuario/'+sessionStorage.getItem('usuario_id'), { headers: this.headers}).map(res=> res.json());
	}

	editUsuario(usuario){
		let jsonempresa = JSON.stringify(usuario); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'usuario/'+sessionStorage.getItem('usuario_id'), jsonempresa, {headers: this.headers})
						  .map(res => res.json());
	}

	addUsuario(usuario){
		let newusuario = JSON.stringify(usuario); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'usuario', newusuario, {headers:this.headers})
	}	
}