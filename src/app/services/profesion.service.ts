import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Profesion } from '../models/profesion';

import { GlobalConfig } from  '../services/global-config';


@Injectable()
export class ProfesionService{

	public url : string;
	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		this.url = GlobalConfig.url + 'profesion';
	}

	getProfesiones(){
		return this._http.get(this.url).map(res=> res.json());
	}

	deleteProfesion(id , profesion:Profesion){
		
		let json = JSON.stringify(profesion);
		let params = json;
		
		

		return this._http.put(this.url + '/' + id, params, {headers: this.headers}).map(res => res.json());
	}


	addProfesion(profesion:Profesion){
		
		let json = JSON.stringify(profesion);
		let params = json;

		return this._http.post(this.url, params, {headers: this.headers}).map(res => res.json());
	}

		updateProfesion(id , profesion:Profesion){
		
		let json = JSON.stringify(profesion);
		let params = json;
		

		return this._http.put(this.url + '/' + id, params, {headers: this.headers}).map(res => res.json());
	}

	getProfesion(id){
		return this._http.get(this.url + '/' + id).map(res => res.json());

	}

}