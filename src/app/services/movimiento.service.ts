import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Movimiento } from '../models/movimiento';
import { GlobalConfig } from  '../services/global-config';


@Injectable()
export class MovimientoService{

	public url: string;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){

		this.url = GlobalConfig.url + 'movimiento';
	}

	getMovimientos(){
		return this._http.get(this.url).map(res=> res.json());
	}

	deleteMovimiento(id){

		return this._http.delete(this.url + '/' + id).map(res => res.json());
	}


	addMovimiento( movimiento:Movimiento){
		
		let json = JSON.stringify(movimiento);
		let params = json;
		

		return this._http.post(this.url, params, {headers:this.headers}).map(res => res.json());
	}

	updateMovimiento(id ,  movimiento:Movimiento){
	
	let json = JSON.stringify(movimiento);
	let params = json;
	

	return this._http.put(this.url + '/' + id, params, {headers: this.headers}).map(res => res.json());
	}

	getMovimiento(id){
		return this._http.get(this.url + '/' + id).map(res => res.json());

	}
}