import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { ConfiguracionPlanilla } from '../models/configuracion_planilla';

import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class ConfiguracionPlanillaService{

	public url : string;
	//private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url+ 'configuracionplanilla/';
	}

	getConfiguracionPlanillas(){
		//return this._http.get(this.url+'configuracionplanilla').map(res=> res.json());

		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
	}

	updateConfiguracionPlanilla(id, configuracionplanilla:ConfiguracionPlanilla){
		let jsonconfigP = JSON.stringify(configuracionplanilla); //Convirtiendo el objeto a JSON
		//console.log(jsonconfigP);
		return this._http.put(this.url, jsonconfigP, {headers: this.headers})
						  .map(res => res.json());
	}

	getConfiguracionPlanilla(id){
		//return this._http.get(this.url + '/' + id).map(res => res.json());

		return this._http.get(this.url , {headers: this.headers}).map(res=> res.json());

	}
}
