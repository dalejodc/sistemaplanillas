import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { GlobalConfig } from  '../services/global-config'
import { Rol } from '../models/rol'

@Injectable()
export class RolService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url;
	}

	getRoles(){
		return this._http.get(this.url+'rol', {headers: this.headers}).map(res=> res.json());
	}

	getRol(id){
		return this._http.get(this.url+'rol/'+id, {headers: this.headers}).map(res=> res.json());
	}

	// getGenerosDeshabilitados(){
	// 	return this._http.get(this.url+'genero/disabled/all').map(res=> res.json());
	// }

	addRol(rol){
		console.log(rol);
		console.log(JSON.stringify(rol));
		let newrol = JSON.stringify(rol); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'rol', newrol, {headers:this.headers})
						 .map(res => res.json());
	}

	editRol(id, rol){
		let jsonrol = JSON.stringify(rol); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'rol/'+ id, jsonrol, {headers: this.headers})
						  .map(res => res.json());
	}
}