import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class EstructuraTerritorialService{

	public url : string;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		this.url = GlobalConfig.url;
	}

	getPaises(){
		return this._http.get(this.url+'estructuraterritorial', {headers: this.headers}).map(res=> res.json());
	}

	getDepartamentos(id){
		return this._http.get(this.url+'estructuraterritorial/'+id, {headers: this.headers}).map(res=> res.json());
	}

	getNiveles(id){
		return this._http.get(this.url+'tipoestructura/'+id, {headers: this.headers}).map(res=> res.json());
	}
}