import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


import { Usuario } from '../models/usuario';
import { Menu } from '../models/menu';
import { GlobalConfig } from  '../services/global-config'


@Injectable()
export class LoginService {

  public url = GlobalConfig.url;
  public token: string;
  public usuario: Usuario;
  public usu: Usuario;
  public opcionesMenu : Menu[]=[];

  public mensaje: string;
  public procesar: boolean;
  public errMsg: boolean;

	constructor(
		private _http: Http,
		private _router: Router
	){
		this.opcionesMenu =[];
		this.usu = new Usuario (null, null);
		this.procesar = true;
	}

  login(usuario: Usuario) {
  	//Objeto auxiliar para poder mandar la peticion desde el sidebar
  	this.usu = usuario;
  	sessionStorage.setItem('user', this.usu.usuario_nombre);
  	sessionStorage.setItem('pass', this.usu.usuario_contrasenia);

	let jsonUser = JSON.stringify(usuario); //Convirtiendo el objeto a JSON
	let headers = new Headers({
	  'Content-type': 'application/json'
	});

	return this._http.post(this.url + 'login', jsonUser, { headers: headers })
	  .map(res => res.json())
	  .subscribe(
		  data => {
		  	this.procesar = true;
			console.log(data);
			this.opcionesMenu = data.menus;
			// console.log(this.opcionesMenu);
			sessionStorage.setItem('usuario', data.usuario);
			sessionStorage.setItem('usuario_id', data.usuario_id);
			sessionStorage.setItem('cant_empleados', data.cantidad_empleados);
			// console.log(data.cantidad_empleados);
			sessionStorage.setItem('rol', data.rol.rol_nombre);
			sessionStorage.setItem('rol_id', data.rol.rol_id);
			sessionStorage.setItem('token', data.token);
			sessionStorage.setItem('empresa', data.rol.empresa_id);
			if(data.usuario_es_nuevo == true){
				sessionStorage.setItem('nuevo', "nuevo");
				this._router.navigate(['/mi-usuario']);
			}else{
				this._router.navigate(['/home']);
			}
		  },
		  error => {
			console.log(error);
			console.log(error._body);
			this.mensaje = error._body;
			this.errMsg = true;
		  }
		);
  } 

	getMenu(){
		return this.opcionesMenu;
	}

	getMensajeError() {
		return this.errMsg;
	}

	getMensaje() {
		return this.mensaje;
	}
}

