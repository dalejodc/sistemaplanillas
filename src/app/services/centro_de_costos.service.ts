import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Abono } from '../models/abono';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class CentroDeCostosService{

	public url : string;


		private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
		});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url + 'centrocosto/';
	}


	getCentroCostos(){
		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
		}

	getCentroCosto(id){
		return this._http.get(this.url + id, {headers: this.headers}).map(res=> res.json());
	}
	
	addAbono(abono: Abono){

		let json = JSON.stringify(abono);
		return this._http.post(this.url + 'abono', json, {headers: this.headers}).map(res=> res.json());

	}
	

}