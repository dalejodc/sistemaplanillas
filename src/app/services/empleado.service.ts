import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Empleado } from '../models/empleado';
import { GlobalConfig } from  '../services/global-config'

@Injectable()
export class EmpladoService{

	public url : string;
	// private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': sessionStorage.getItem('token')
	});

	constructor(
		private _http: Http
	){
		//console.log(sessionStorage.getItem('token'));
		this.url = GlobalConfig.url + 'empleado/';
	}

	getEmpleados(){
		return this._http.get(this.url, {headers: this.headers}).map(res=> res.json());
	}

	getEmpleado(id){
		return this._http.get(this.url+id, {headers: this.headers}).map(res=> res.json());
	}

	addEmpleado(empleado : Empleado){


		let newEmpleado = JSON.stringify(empleado); //Convirtiendo el objeto a JSON
		console.log(newEmpleado);

		return this._http.post(this.url, newEmpleado, {headers:this.headers})
						 .map(res => res.json());
	}

	updateEmpleado(id, empleado: Empleado){
		let jsonEmplado = JSON.stringify(empleado); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+ id, jsonEmplado, {headers: this.headers})
						  .map(res => res.json());
	}
	deleteEmpleado(id){
		return this._http.delete(this.url + id, {headers:this.headers}).map(res=> res.json());

	}
}