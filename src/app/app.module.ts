import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterializeModule } from 'angular2-materialize';
import { SweetAlertService } from 'angular-sweetalert-service';
import { routing, appRoutingProviders} from './app.routing';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { ChartsModule } from 'ng2-charts';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login.component';
import { HomeComponent } from './components/home.component';
import { PlanComponent } from './components/planes.component';
import { PlanAddComponent } from './components/planes-add.component';
import { PlanEditComponent } from './components/planes-edit.component';
import { RolComponent } from './components/rol.component';
import { RolAddComponent } from './components/rol-add.component';
import { RolEditComponent } from './components/rol-edit.component';
import { RentaComponent } from './components/renta.component';
import { RentaAddComponent } from './components/renta-add.component';
import { RentaEditComponent } from './components/renta-edit.component';
import { InfoEmpresaComponent } from './components/info-empresa.component';
import { InfoEmpresaEditComponent } from './components/info-empresa-edit.component';
import { ErrorComponent } from './components/error.component';
import { SidebarComponent } from './components/sidebar.component';
import { NavbarComponent } from './components/navbar.component';
import { VentajasComponent } from './components/ventajas.component';
import { RegistroComponent } from './components/registro.component';
import { EmpresaListComponent } from './components/empresa.component';
import { EmpresaAddComponent } from './components/empresa-add.component';
import { EmpresaSolicitudAddComponent } from './components/empresa-solicitud-add.component';
import { EmpresaEditComponent } from './components/empresa-edit.component';
import { EstadoCivilComponent } from './components/estado-civil.component';
import { EstadoCivilAddComponent } from './components/estado-civil-add.component';
import { EstadoCivilEditComponent } from './components/estado-civil-edit.component';
import { GeneroComponent } from './components/genero.component';
import { GeneroAddComponent } from './components/genero-add.component';
import { GeneroEditComponent } from './components/genero-edit.component';
import { TipoDocumentoComponent } from './components/tipo-documento.component';
import { TipoDocumentoAddComponent } from './components/tipo-documento-add.component';
import { TipoDocumentoEditComponent } from './components/tipo-documento-edit.component';
import { TipoCorreoComponent } from './components/tipo_correo.component';
import { TipoCorreoAddComponent } from './components/tipo-correo-add.component';
import { TipoCorreoEditComponent } from './components/tipo-correo-edit.component';
import { TipoTelefonoComponent } from './components/tipo-telefono.component';
import { TipoTelefonoAddComponent } from './components/tipo-telefono-add.component';
import { TipoTelefonoEditComponent } from './components/tipo-telefono-edit.component';
import { TipoContratoComponent } from './components/tipo-contrato.component';
import { TipoContratoAddComponent } from './components/tipo-contrato-add.component';
import { TipoContratoEditComponent } from './components/tipo-contrato-edit.component';
import { ContratoComponent } from './components/contrato.component';
import { ContratoAddComponent } from './components/contrato-add.component';
import { ProfesionComponent } from './components/profesion.component';
//import { Loader } from './components/loader.component';
import { AyudaComponent } from './components/ayuda.component';

import { ProfesionAdd } from './components/profesion-add.component';
import { ProfesionUpdate } from './components/profesion-update.component';
import { MovimientoComponent } from './components/movimiento.component';
import { TipoTransaccionComponent } from './components/tipoTransaccion.component';
import { TipoTransaccionAdd } from './components/tipoTransaccion-add.component';
import { TipoTransaccionUpdate } from './components/tipoTransaccion-update.component';
import { MovimientoAdd } from './components/movimiento-add.component';
import { MovimientoUpdate } from './components/movimiento-update.component';
import { NivelEstructuraComponent } from './components/nivel-estructura.component';
import { NivelEstructuraAddComponent } from './components/nivel-estructura-add.component';
import { ComisionComponent } from './components/comision.component';
import { ComisionAdd } from './components/comision-add.component';
import { ComisionUpdate } from './components/comision-update.component';
import { PlanillaGetComponent } from './components/planilla-get.component';
import { DetalleMovimientoAdd } from './components/detalleMovimientoAdd.component';
import { EmpleadoComponent } from './components/empleado.component';
import { EmpleadoAdd } from './components/empleado-add.component';
import { VerplanillaComponent } from './components/ver-planillas.component';
import { MiUsuarioComponent } from './components/mi-usuario.component';
import { GetEmpleado } from './components/get-empleado.component';
import { EmpleadoUsuarioComponent } from './components/empleado-usuario.component';
import { EmpleadoUsuarioAddComponent } from './components/empleado-usuario-add.component';

import { PuestoComponent } from './components/puesto.component';
import { PuestoAdd } from './components/puesto-add.component';
import { PuestoUpdate } from './components/puesto-update.component';
import { CategoriaSalarioComponent } from './components/categoriaSalario.component';
import { CategoriaSalarioAdd } from './components/categoriaSalario-add.component';
import { CategoriaSalarioUpdate } from './components/categoriaSalario-update.component';

import { ConfiguracionPlanillaUpdate } from './components/configuracionPlanilla-update.component';
import { ConfiguracionPlanillaComponent } from './components/configuracionPlanilla.component';

import { TipoSalarioComponent } from './components/tipo-salario.component';
import { TipoSalarioAdd } from './components/tipo-salario-add.component';
import { TipoSalarioUpdate } from './components/tipo-salario-update.component';
import { VentaAdd } from './components/venta-add';
import { CentroDeCostosComponent } from './components/centro_de_costos.component';
import { AbonoAdd } from './components/abonar.component';
import { GetCentroCosto} from './components/centro_de_costos-get.component';
import { Chart } from './components/grafica';

import { AuthGuardService } from './services/guard.service';

 
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PlanComponent,
    PlanAddComponent,
    PlanEditComponent,

    AyudaComponent,

    RolComponent,
    RolAddComponent,
    RolEditComponent,
    RentaComponent,
    RentaAddComponent,
    RentaEditComponent,
    VentajasComponent,
    InfoEmpresaComponent,
    InfoEmpresaEditComponent,
    EstadoCivilComponent,
    EstadoCivilAddComponent,
    EstadoCivilEditComponent,
    GeneroComponent,
    GeneroAddComponent,
    GeneroEditComponent,
    TipoDocumentoComponent,
    TipoDocumentoAddComponent,
    TipoDocumentoEditComponent,
    TipoCorreoComponent,
    TipoCorreoAddComponent,
    TipoCorreoEditComponent,
    TipoTelefonoComponent,
    TipoTelefonoAddComponent,
    TipoTelefonoEditComponent,
    TipoContratoComponent,
    TipoContratoAddComponent,
    TipoContratoEditComponent,
    ContratoComponent,
    ContratoAddComponent,
    SidebarComponent,
    NavbarComponent,
    RegistroComponent,
    EmpresaListComponent,
    EmpresaAddComponent,
    EmpresaSolicitudAddComponent,
    EmpresaEditComponent,
    ErrorComponent,
    ProfesionComponent,
    ProfesionAdd,
    ProfesionUpdate,
    EmpleadoUsuarioComponent,
    EmpleadoUsuarioAddComponent,

    MovimientoComponent,
    TipoTransaccionComponent,
    TipoTransaccionAdd,
    TipoTransaccionUpdate,
    MovimientoAdd,
    MovimientoUpdate,
    ProfesionUpdate,
    NivelEstructuraComponent,
    NivelEstructuraAddComponent,
    ProfesionUpdate,
    ComisionComponent,
    ComisionAdd,
    ComisionUpdate,
    PlanillaGetComponent,
    DetalleMovimientoAdd,
    EmpleadoComponent,
    EmpleadoAdd,
    VerplanillaComponent,
    MiUsuarioComponent,
    GetEmpleado,
    VentaAdd,
    CentroDeCostosComponent,
    AbonoAdd,
    GetCentroCosto,
    Chart,


    
    PuestoComponent,
    PuestoAdd,
    PuestoUpdate,
    CategoriaSalarioComponent,
    CategoriaSalarioAdd,
    CategoriaSalarioUpdate,

    TipoSalarioComponent,
    TipoSalarioAdd,
    TipoSalarioUpdate,

    ConfiguracionPlanillaUpdate,
    ConfiguracionPlanillaComponent

  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    routing,
    HttpModule,
    FormsModule,
    ChartsModule,
 DataTablesModule
  ],
  providers: [appRoutingProviders, SweetAlertService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
