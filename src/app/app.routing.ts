/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule} from "@angular/router";

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { LoginComponent } from './components/login.component';
import { ErrorComponent } from './components/error.component';
import { RegistroComponent } from './components/registro.component';
import { EmpresaListComponent } from './components/empresa.component';
import { EmpresaAddComponent } from './components/empresa-add.component';
import { EmpresaEditComponent } from './components/empresa-edit.component';
import { HomeComponent } from './components/home.component';
import { PlanComponent } from './components/planes.component';
import { PlanAddComponent } from './components/planes-add.component';
import { PlanEditComponent } from './components/planes-edit.component';
import { RolComponent } from './components/rol.component';
import { RolAddComponent } from './components/rol-add.component';
import { RolEditComponent } from './components/rol-edit.component';
import { RentaComponent } from './components/renta.component';
import { RentaAddComponent } from './components/renta-add.component';
import { RentaEditComponent } from './components/renta-edit.component';
import { InfoEmpresaComponent } from './components/info-empresa.component';
import { InfoEmpresaEditComponent } from './components/info-empresa-edit.component';
import { EstadoCivilComponent } from './components/estado-civil.component';
import { EstadoCivilAddComponent } from './components/estado-civil-add.component';
import { EstadoCivilEditComponent } from './components/estado-civil-edit.component';
import { GeneroComponent } from './components/genero.component';
import { GeneroAddComponent } from './components/genero-add.component';
import { GeneroEditComponent } from './components/genero-edit.component';
import { TipoDocumentoComponent } from './components/tipo-documento.component';
import { TipoDocumentoAddComponent } from './components/tipo-documento-add.component';
import { TipoDocumentoEditComponent } from './components/tipo-documento-edit.component';
import { TipoCorreoComponent } from './components/tipo_correo.component';
import { TipoCorreoAddComponent } from './components/tipo-correo-add.component';
import { TipoCorreoEditComponent } from './components/tipo-correo-edit.component';
import { TipoTelefonoComponent } from './components/tipo-telefono.component';
import { TipoTelefonoAddComponent } from './components/tipo-telefono-add.component';
import { TipoTelefonoEditComponent } from './components/tipo-telefono-edit.component';
import { TipoContratoComponent } from './components/tipo-contrato.component';
import { TipoContratoAddComponent } from './components/tipo-contrato-add.component';
import { TipoContratoEditComponent } from './components/tipo-contrato-edit.component';
import { ContratoComponent } from './components/contrato.component';
import { ContratoAddComponent } from './components/contrato-add.component';
import { ProfesionComponent} from './components/profesion.component';
import { ProfesionAdd } from './components/profesion-add.component';
import { ProfesionUpdate } from './components/profesion-update.component';
import { MovimientoComponent } from './components/movimiento.component';
import { TipoTransaccionComponent } from './components/tipoTransaccion.component';
import { TipoTransaccionAdd } from './components/tipoTransaccion-add.component';
import { TipoTransaccionUpdate } from './components/tipoTransaccion-update.component';
import { MovimientoAdd } from './components/movimiento-add.component';
import { MovimientoUpdate } from './components/movimiento-update.component';
import { NivelEstructuraComponent } from './components/nivel-estructura.component';
import { NivelEstructuraAddComponent } from './components/nivel-estructura-add.component';
import { ComisionComponent } from './components/comision.component';
import { ComisionAdd } from './components/comision-add.component';
import { ComisionUpdate } from './components/comision-update.component';
import { PlanillaGetComponent } from './components/planilla-get.component';
import { DetalleMovimientoAdd } from './components/detalleMovimientoAdd.component';
import { EmpleadoComponent } from './components/empleado.component';
import { EmpleadoAdd } from './components/empleado-add.component';
import { VerplanillaComponent } from './components/ver-planillas.component';
import { MiUsuarioComponent } from './components/mi-usuario.component';
import { GetEmpleado } from './components/get-empleado.component';
import { EmpleadoUsuarioComponent } from './components/empleado-usuario.component';
import { EmpleadoUsuarioAddComponent } from './components/empleado-usuario-add.component';
import { VentaAdd } from './components/venta-add';
import { AyudaComponent } from './components/ayuda.component';

import { PuestoComponent } from './components/puesto.component';
import { PuestoAdd } from './components/puesto-add.component';
import { PuestoUpdate } from './components/puesto-update.component';
import { CategoriaSalarioComponent } from './components/categoriaSalario.component';
import { CategoriaSalarioAdd } from './components/categoriaSalario-add.component';
import { CategoriaSalarioUpdate } from './components/categoriaSalario-update.component';
import { TipoSalarioComponent } from './components/tipo-salario.component';
import { TipoSalarioAdd } from './components/tipo-salario-add.component';
import { TipoSalarioUpdate } from './components/tipo-salario-update.component';
import { CentroDeCostosComponent } from './components/centro_de_costos.component';
import { AbonoAdd } from './components/abonar.component';
import { GetCentroCosto} from './components/centro_de_costos-get.component';


import { AuthGuardService } from './services/guard.service';


import { ConfiguracionPlanillaComponent } from './components/configuracionPlanilla.component';
import { ConfiguracionPlanillaUpdate } from './components/configuracionPlanilla-update.component';
/*=============================================================================================
              ARREGLO DE LAS RUTAS
===============================================================================================*/
const appRoutes: Routes = [
	{path: '', component: LoginComponent}, //Página Home, ruta inicial
	
	{path: 'home', component: HomeComponent, canActivate:[AuthGuardService]},
	{path: 'registro', component: RegistroComponent, canActivate:[AuthGuardService]},
	{path: 'mi-empresa', component: InfoEmpresaComponent, canActivate:[AuthGuardService]},
	{path: 'mi-usuario', component: MiUsuarioComponent, canActivate:[AuthGuardService]},
	{path: 'mi-empresa-edit/:id', component: InfoEmpresaEditComponent, canActivate:[AuthGuardService]},
	{path: 'planes', component: PlanComponent, canActivate:[AuthGuardService]},
	{path: 'plan-add', component: PlanAddComponent, canActivate:[AuthGuardService]},
	{path: 'plan-edit/:id', component: PlanEditComponent, canActivate:[AuthGuardService]},
	{path: 'roles', component: RolComponent, canActivate:[AuthGuardService]},
	{path: 'rol-add', component: RolAddComponent, canActivate:[AuthGuardService]},
	{path: 'rol-edit/:id', component: RolEditComponent, canActivate:[AuthGuardService]},
	{path: 'renta', component: RentaComponent, canActivate:[AuthGuardService]},
	{path: 'renta-add', component: RentaAddComponent, canActivate:[AuthGuardService]},
	{path: 'renta-edit/:id', component: RentaEditComponent, canActivate:[AuthGuardService]},
	{path: 'empresa', component: EmpresaListComponent, canActivate:[AuthGuardService]},
	{path: 'empresa-add', component: EmpresaAddComponent, canActivate:[AuthGuardService]},
	{path: 'empresa-edit/:id', component: EmpresaEditComponent, canActivate:[AuthGuardService]},
	{path: 'estado-civil', component: EstadoCivilComponent, canActivate:[AuthGuardService]},
	{path: 'estado-civil-add', component: EstadoCivilAddComponent, canActivate:[AuthGuardService]},
	{path: 'estado-civil-edit/:id', component: EstadoCivilEditComponent, canActivate:[AuthGuardService]},
	{path: 'genero', component: GeneroComponent, canActivate:[AuthGuardService]},
	{path: 'genero-add', component: GeneroAddComponent, canActivate:[AuthGuardService]},
	{path: 'genero-edit/:id', component: GeneroEditComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-documento', component: TipoDocumentoComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-documento-add', component: TipoDocumentoAddComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-documento-edit/:id', component: TipoDocumentoEditComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-correo', component: TipoCorreoComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-correo-add', component: TipoCorreoAddComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-correo-edit/:id', component: TipoCorreoEditComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-telefono', component: TipoTelefonoComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-telefono-add', component: TipoTelefonoAddComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-telefono-edit/:id', component: TipoTelefonoEditComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-contrato', component: TipoContratoComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-contrato-add', component: TipoContratoAddComponent, canActivate:[AuthGuardService]},
	{path: 'tipo-contrato-edit/:id', component: TipoContratoEditComponent, canActivate:[AuthGuardService]},
	{path: 'contrato', component: ContratoComponent, canActivate:[AuthGuardService]},
	{path: 'contrato-add', component: ContratoAddComponent, canActivate:[AuthGuardService]},
	{path: 'profesion', component: ProfesionComponent, canActivate:[AuthGuardService]},
	{path: 'profesion-add', component: ProfesionAdd},
	{path: 'profesion-update/:id', component: ProfesionUpdate},
	
	{path: 'puesto', component: PuestoComponent, canActivate:[AuthGuardService]},
	{path: 'ayuda', component: AyudaComponent, canActivate:[AuthGuardService]},
	{path: 'puesto-add', component: PuestoAdd, canActivate:[AuthGuardService]},
	{path: 'puesto-update/:id', component: PuestoUpdate, canActivate:[AuthGuardService]},
	{path: 'categoriaSalario', component: CategoriaSalarioComponent, canActivate:[AuthGuardService]},
	{path: 'categoriaSalario-add', component: CategoriaSalarioAdd, canActivate:[AuthGuardService]},
	{path: 'categoriaSalario-update/:id', component: CategoriaSalarioUpdate, canActivate:[AuthGuardService]},
	{path: 'tipoSalario', component: TipoSalarioComponent, canActivate:[AuthGuardService]},
	{path: 'tipoSalario-add', component: TipoSalarioAdd, canActivate:[AuthGuardService]},
	{path: 'tipoSalario-update/:id', component: TipoSalarioUpdate, canActivate:[AuthGuardService]},

	{path: 'movimiento', component: MovimientoComponent, canActivate:[AuthGuardService]},
	{path: 'movimiento-add', component: MovimientoAdd, canActivate:[AuthGuardService]},
	{path: 'movimiento-update/:id', component: MovimientoUpdate, canActivate:[AuthGuardService]},
	{path: 'tipotransaccion', component: TipoTransaccionComponent, canActivate:[AuthGuardService]},
	{path: 'tipotransaccion-add', component: TipoTransaccionAdd, canActivate:[AuthGuardService]},
	{path: 'tipotransaccion-update/:id', component: TipoTransaccionUpdate, canActivate:[AuthGuardService]},
	{path: 'nivel-estructura', component: NivelEstructuraComponent, canActivate:[AuthGuardService]},
	{path: 'nivel-estructura-add', component: NivelEstructuraAddComponent, canActivate:[AuthGuardService]},
	{path: 'comision', component: ComisionComponent, canActivate:[AuthGuardService]},
	{path: 'comision-add', component: ComisionAdd, canActivate:[AuthGuardService]},
	{path: 'comision-update/:id', component: ComisionUpdate, canActivate:[AuthGuardService]},

	{path: 'planilla/:id', component: PlanillaGetComponent, canActivate:[AuthGuardService]},
	{path: 'periodoplanilla', component: VerplanillaComponent, canActivate:[AuthGuardService]},
	{path: 'detalle-movimiento-add', component: DetalleMovimientoAdd, canActivate:[AuthGuardService]},
	{path: 'empleado', component: EmpleadoComponent, canActivate:[AuthGuardService]},
	{path: 'empleado-add', component: EmpleadoAdd, canActivate:[AuthGuardService]},
	{path: 'get-empleado/:id', component: GetEmpleado, canActivate:[AuthGuardService]},
	{path: 'empleado-usuario', component: EmpleadoUsuarioComponent, canActivate:[AuthGuardService]},
	{path: 'empleado-usuario-add/:id', component: EmpleadoUsuarioAddComponent, canActivate:[AuthGuardService]},
	{path: 'venta-add', component: VentaAdd, canActivate:[AuthGuardService]},
	{path: 'centrocosto', component: CentroDeCostosComponent, canActivate:[AuthGuardService]},
	{path: 'abonar/:id', component: AbonoAdd, canActivate:[AuthGuardService]},
	{path: 'get-centrocosto/:id', component: GetCentroCosto, canActivate:[AuthGuardService]},






	{path: 'configuracionPlanilla-update/:id', component: ConfiguracionPlanillaUpdate, canActivate:[AuthGuardService]},
	{path: 'configuracionPlanilla', component: ConfiguracionPlanillaComponent, canActivate:[AuthGuardService]},

	{path: '**', component: ErrorComponent} //Página para errores, cuando la ruta falla
]


//Procedimiento que necesita Angular
export const appRoutingProviders: any[] = [];
/*
-Variable routing de tipo ModuleWithProviders
-Con el método .forRoot le decimos qué arreglo tiene que cargar para las rutas, tiene que ser el
que habíamos creado antes. 
-Agara todas las rutas que le hemos indicado, las introduzca y las inyecte en la configuración de rutas del 
framework */
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);