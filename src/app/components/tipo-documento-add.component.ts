import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { TipoDocumentoService } from '../services/tipo-documento.service';
import { TipoDocumento } from '../models/tipo_documento';

@Component({
	selector: 'tipo-correo-add',
	templateUrl: '../views/tipo-documento-add.component.html',
	providers: [TipoDocumentoService]
})

export class TipoDocumentoAddComponent {

	public tipoDocumento: TipoDocumento;

	constructor(
		private _tipoDocumentoService: TipoDocumentoService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.tipoDocumento= new TipoDocumento (null, null, null, null, true);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		console.log(this.tipoDocumento);
	}

	guardarDocumento(){
		console.log(this.tipoDocumento);
		console.log(JSON.stringify(this.tipoDocumento));
		this._tipoDocumentoService.addCorreo(this.tipoDocumento).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El tipo de documento ' + this.tipoDocumento.tipo_documento_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/tipo-documento']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}