import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { TipoContratoService } from '../services/tipo-contrato.service';
import { ContratoService } from '../services/contrato.service';
import { Contrato } from '../models/contrato';
import { TipoContrato } from '../models/tipo_contrato';

@Component({
	selector: 'contrato',
	templateUrl: '../views/contrato-add.component.html',
	providers: [ContratoService, TipoContratoService]
})

export class ContratoAddComponent {

	public contrato: Contrato;
	public listaTiposContratos : TipoContrato[];
	public id: number;
	public duracion: number;
	public descripcion: string;

	constructor(
		private _contatoService: ContratoService,
		private _tipoContatoService: TipoContratoService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.contrato= new Contrato ( null, null, null, null, true);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getTiposContratos();
	}

	getTiposContratos(){
		this._tipoContatoService.getTiposContratos().subscribe(
			result => {
				this.listaTiposContratos = result;
				console.log(this.listaTiposContratos);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	guardarContrato(){
		this.contrato.forma_contrato_id = this.id;
		this.contrato.contrato_descripcion = this.descripcion;
		this.contrato.contrato_duracion = this.duracion;
		console.log(JSON.stringify(this.contrato));

		this._contatoService.addContrato(this.contrato).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El contrato ' + this.contrato.contrato_descripcion+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/contrato']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}