import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Usuario } from '../models/usuario';
import { LoginService } from '../services/login.service';

@Component({
	selector: 'login',
	templateUrl: '../views/login.component.html',
	providers: [LoginService]
})

export class LoginComponent{

	public usuario: Usuario;
	public errMsg: boolean;
	public advertencia="Error, las credenciales son incorrectas.";
	public baneado: string;
	public procesar: boolean;


	constructor(
		private _loginService: LoginService,
	){
		this.usuario = new Usuario("", "");
		this.procesar= false;
	}

	ngOnInit(){
		this.usuario = new Usuario("", "");
	}

	IniciarSesion(){
		// this.errMsg = false;
		// this.procesar = true;
		sessionStorage.setItem("usuario", this.usuario.usuario_nombre);
		this._loginService.login(this.usuario);
		this.errMsg = this._loginService.getMensajeError();
		this.baneado = "Usuario: " + " " + sessionStorage.getItem("usuario")+"," + " " +this._loginService.getMensaje();
		//console.log(this.errMsg);
		if(this.errMsg) {		//Si ocurre un error, reinicia las credenciales
			this.usuario = new Usuario(null, null);
			this.procesar = false;
			// this.errMsg = false;
		}
	}
}
