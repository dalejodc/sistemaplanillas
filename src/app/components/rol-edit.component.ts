import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { RolService } from '../services/rol.service';
import { Rol } from '../models/rol';
import { Menu } from '../models/menu';
import { Vista } from '../models/vista';

import { MenuService } from '../services/menu.service';

@Component({
	selector: 'rol-edit',
	templateUrl: '../views/rol-edit.component.html',
	providers: [RolService, MenuService]
})

export class RolEditComponent{
	public rol : Rol;
	public listaMenus: Menu;
	public asignacionRolMenu :Vista[]

	constructor(
		private _route: ActivatedRoute,
		private _menuService: MenuService,
		private _router: Router,
		private _rolService: RolService,
		private alertService: SweetAlertService	
	){
		this.rol = new Rol(null,null, null, null, null);
		this.asignacionRolMenu = [];
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getMenus();
		this.getRol();
	}

	//Método para verificar la selección o deselección del box
	onCheckboxChange(id, event) {
		if(event.target.checked) { //si es seleccionado ingresa el objeto al arreglo con el id
		  this.asignacionRolMenu.push({"menu_id": id});
		  console.log('Seleccionó:' ,this.asignacionRolMenu);
		} else {
		//De lo contrario, si es desmarcado, hay que recorrer el arreglo para quitar el 
		//elemento 
		  for (let x in this.asignacionRolMenu){
				let index = this.asignacionRolMenu.findIndex(x => x.menu_id == id);
				this.asignacionRolMenu.splice(index, 1);
				console.log('Deseleccionó:' ,this.asignacionRolMenu);
		  	 }
		  }
		}

	getMenus(){
		this._menuService.getMenus().subscribe(
			result=>{
				this.listaMenus = result;
				console.log(this.listaMenus);
			},
			error=>{
				console.log(<any>error);
			}
			);
	}

	comparacion(){

		for (let x in this.asignacionRolMenu){

			for(let y in this.listaMenus){
				if(this.asignacionRolMenu[x].menu_id == this.listaMenus[y].menu_id){
					console.log("Debería estar marcado:", this.listaMenus[y].menu_nombre);
					$("#"+ String(this.listaMenus[y].menu_id)).prop('checked', true);		
				}
			}
		}
	}

	getRol(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._rolService.getRol(id).subscribe(
				response=>{
						this.rol= response;
						this.asignacionRolMenu = response.asignacionRolMenu;
						this.comparacion();
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarRol(){

		interface ROLL {
			    rol_nombre: string,
			    rol_descripcion : string,
			    rol_estado : true,
			    asignacionRolMenu : Vista[]
			};

		//Creando objeto auxiliar para que sea aceptado en el formato de la URL
		const ROLL : ROLL = {
			rol_nombre : this.rol.rol_nombre,  
			rol_descripcion : this.rol.rol_descripcion, 
			rol_estado: true, 
			asignacionRolMenu : this.asignacionRolMenu
		};

		console.log(JSON.stringify(this.asignacionRolMenu));

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._rolService.editRol(id, ROLL).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El Rol ' + this.rol.rol_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/roles']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}