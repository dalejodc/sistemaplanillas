import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { RolService } from '../services/rol.service';
import { Rol } from '../models/rol';

@Component({
	selector: 'roles',
	templateUrl: '../views/roles-list.component.html',
	providers: [RolService]
})

export class RolComponent {

	public listaRoles : Rol[];
	public rol: Rol;

	constructor(
		private _rolService: RolService,
		private alertService: SweetAlertService	
	){}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getRoles();
	}

	getRoles(){
		this._rolService.getRoles().subscribe(
			result => {
				this.listaRoles = result;
				console.log(this.listaRoles);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	// deshabilitar(genero){
	// 	this.alertService.confirm({
	//       title: '¿Desea deshabilitar el género '+genero.genero_nombre+'?',
	//       text: "El género se deshabilitará y no podrá asignarse a los empleados.",
	//       cancelButtonColor: '#E57373',
 //  		  confirmButtonColor: '#83C283',
 //  		  confirmButtonText: 'Aceptar'
	//     })
	//     .then(() => {
	// 	    this.genero = genero;
	// 		this.genero.genero_estado =false;

	// 		this._generoService.editGenero(genero.genero_id, this.genero).subscribe(
	// 			response =>{
	// 				this.alertService.success({
	//         			title: '¡El género ' + this.genero.genero_nombre+ ' fue deshabilitado exitosamente!'
	//       			});
	// 				this.ngOnInit();
	// 			},
	// 			error =>{
	// 				this.alertService.error({
	//         			title: '¡Ocurrió un error!'
	//       			});
	// 				console.log(<any>error);
	// 			}
	// 		);	
	//     })
	//     .catch(() => console.log('canceled'));
	// }
}