import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { TipoContratoService } from '../services/tipo-contrato.service';
import { TipoContrato } from '../models/tipo_contrato';

@Component({
  selector: 'tipo-contrato-add',
  templateUrl: '../views/tipo-contrato-add.component.html',
  providers: [TipoContratoService]
})
export class TipoContratoAddComponent implements OnInit {

  public tipoContrato: TipoContrato;

	constructor(
		private _tipoContratoService: TipoContratoService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.tipoContrato= new TipoContrato ( null, null, null, true);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
	}

	guardarTipoContrato(){
		console.log(this.tipoContrato);

		this._tipoContratoService.addTipoContrato(this.tipoContrato).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El tipo de contrato ' + this.tipoContrato.forma_contrato_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/tipo-contrato']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}

}
