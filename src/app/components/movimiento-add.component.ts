import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { Movimiento } from '../models/movimiento'
import { TipoTransaccion } from '../models/tipo_transaccion'

import { MovimientoService } from '../services/movimiento.service'
import { TipoTransaccionService } from '../services/tipo_transaccion.service'
declare var $:any;
declare var Materialize:any;



@Component({
	selector: 'movimiento-add',
	templateUrl: '../views/movimientto-add.component.html',
	providers: [MovimientoService, TipoTransaccionService]
})

export class MovimientoAdd{
	public movimiento: Movimiento;
	public tipoTransaccion: TipoTransaccion;
	public listaTipoTransaccion: TipoTransaccion[];

	constructor(
		private _movimientoService: MovimientoService,
		private _tipoTransaccionService: TipoTransaccionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){

		this.tipoTransaccion = new TipoTransaccion(null,null,null,null,null);
		//this.movimiento = new Movimiento(0, this.tipoTransaccion,'','');
		this.movimiento = new Movimiento(0, this.tipoTransaccion.tipo_transaccion_id, null ,'','',false);
 
	}

	ngOnInit(){
		this.getTipoTransacciones();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getTipoTransacciones(){
		this._tipoTransaccionService.getTipoTransacciones().subscribe(
			response =>{
				this.listaTipoTransaccion = response;
				console.log(this.listaTipoTransaccion);

			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	onSubmit(){
		console.log(this.movimiento);
		


		this._movimientoService.addMovimiento(this.movimiento).subscribe(
			response=>{

				this.alertService.success({
	        			title: '¡El movimiento ' + this.movimiento.movimiento_nombre+ ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/movimiento']);

			},
			error => {
				console.log(error);
				this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text: 'El movimiento ya existe'
	      		});


			}
			);

	}


}