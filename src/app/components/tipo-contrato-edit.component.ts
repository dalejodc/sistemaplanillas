import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { TipoContratoService } from '../services/tipo-contrato.service';
import { TipoContrato } from '../models/tipo_contrato';

@Component({
	selector: 'estado-civil-edit',
	templateUrl: '../views/tipo-contrato-add.component.html',
	providers: [TipoContratoService]
})

export class TipoContratoEditComponent{
	public tipoContrato : TipoContrato;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tipoContratoService: TipoContratoService,
		private alertService: SweetAlertService	
	){
		this.tipoContrato = new TipoContrato(null,null, null, true);
	}

	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		$(document).ready(function() { Materialize.updateTextFields(); });
		this.getTipoContrato();
	}

	getTipoContrato(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			// console.log(id);

			this._tipoContratoService.getTipoContrato(id).subscribe(
				response=>{
						this.tipoContrato= response;
						console.log(this.tipoContrato);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarTipoContrato(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._tipoContratoService.editTipoContrato(id, this.tipoContrato).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El tipo de contrato ' + this.tipoContrato.forma_contrato_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/tipo-contrato']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}