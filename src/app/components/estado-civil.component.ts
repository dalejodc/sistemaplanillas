import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Subject } from 'rxjs/Rx';
declare var $:any;
declare var Materialize:any;

import { EstadoCivilService } from '../services/estado-civil.service';
import { EstadoCivil } from '../models/estado_civil';

@Component({
	selector: 'estado-civil',
	templateUrl: '../views/estado-civil-list.component.html',
	providers: [EstadoCivilService]
})

export class EstadoCivilComponent implements OnInit {

	public listaEstadosCiviles : EstadoCivil[];
	public estadoCivil: EstadoCivil;

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _estadocivilService: EstadoCivilService,
		private alertService: SweetAlertService	
	){
		this.dtOptions = {
		    pageLength: 10,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getEstadosCiviles();
	}

	getEstadosCiviles(){
		this._estadocivilService.getEstadoCiviles().subscribe(
			result => {
				this.listaEstadosCiviles = result;
				console.log(this.listaEstadosCiviles);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	deshabilitar(estadoCivil){
		this.alertService.confirm({
	      title: '¿Desea deshabilitar el estado civil '+estadoCivil.estado_civil_nombre+'?',
	      text: "El estado civil se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
			// console.log(estadoCivil)
		    this.estadoCivil = estadoCivil;
			// this.estadoCivil.estado_civil_estado =false;
			// console.log(estadoCivil)

			this._estadocivilService.deleteEstadoCivil(estadoCivil.estado_civil_id).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El estado civil ' + this.estadoCivil.estado_civil_nombre+ ' fue deshabilitado exitosamente!'
	      			});
					this.ngOnInit();
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);	
	    })
	    .catch(() => console.log('canceled'));
	    console.log(estadoCivil)
	}
}