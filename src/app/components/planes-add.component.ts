import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { PlanService } from '../services/plan.service';
import { Plan } from '../models/plan';

@Component({
	selector: 'plan-add',
	templateUrl: '../views/plan-add.component.html',
	providers: [PlanService]
})

export class PlanAddComponent {

	public plan: Plan;

	constructor(
		private _planService: PlanService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.plan= new Plan ( null, null, null, null);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
	}

	guardarPlan(){
		console.log(this.plan);

		this._planService.addPlan(this.plan).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El plan ' + this.plan.plan_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/planes']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
				console.log(error.data);
			}
		);
	}
}