import { Component } from '@angular/core';

@Component({
	selector: 'error',
	templateUrl: '../views/error.component.html'
})

export class ErrorComponent {
	public titulo: string;
	public subTitulo: string;

	constructor(){
		this.titulo = "¡Ups!",
		this.subTitulo= "La página solicitada no existe"
	}
	
	ngOnInit(){
		
	}
}