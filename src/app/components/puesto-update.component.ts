import { Component } from '@angular/core'

import { Puesto } from '../models/puesto'
import { NivelEstructura } from '../models/nivel_estructura'

import { PuestoService} from '../services/puesto.service'
import { NivelEstructuraService } from '../services/nivel-estructura.service'

import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'

declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'puesto-update',
	templateUrl : '../views/puesto-update.component.html',
	providers : [ NivelEstructuraService,PuestoService]
})

export class PuestoUpdate{
	public p : Puesto;
	public nivelEstructura: NivelEstructura;
	public listnivelEstructura: NivelEstructura[];

	constructor(
		private _puestoService: PuestoService,
	  	private _nivelEstructuraService: NivelEstructuraService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		//this.p = new Puesto(null,null,'','', true, false);
		//this.p = new Puesto(null,this.nivelEstructura.nivel_estructura_id,null,'','', true,false);


	}

	ngOnInit(){
	  this.getNivelesEstructura();
		$(document).ready(function() { Materialize.updateTextFields(); });
		this.getPuesto();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	
	getNivelesEstructura(){
		this._nivelEstructuraService.getNivelesEstructura().subscribe(
			response =>{
				this.listnivelEstructura = response;
				console.log(this.listnivelEstructura);

			},
			error=>{
				console.log(<any>error);
			}

		);
	}
	

	getPuesto(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._puestoService.getPuesto(id).subscribe(
				response =>{
					this.p = response;		
				},
				error => {
				})

		});
	}

	onCheckboxChange(id, event) {
		if(event.target.checked) {
		this.p.puesto_tiene_comision=true;
		 console.log('Seleccionó:' ,this.p.puesto_tiene_comision); 
		} else {
		
		  	 this.p.puesto_tiene_comision=false;
		  	 console.log('Deseleccionó:' ,this.p.puesto_tiene_comision);
		  }
		}

		

	onSubmit(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

		this._puestoService.updatePuesto(id, this.p).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El puesto ' + this.p.puesto_nombre+ ' fue editado exitosamente!'
	      			});
					this._router.navigate(['/puesto']);
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'});

				}
			);
	
		});

	}
}