import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { ConfiguracionPlanilla } from '../models/configuracion_planilla'
import { Periodo } from '../models/periodo'

import { ConfiguracionPlanillaService } from '../services/configuracion_planilla.service'
import { PeriodoService } from '../services/periodo.service'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'configuracionPlanilla',
	templateUrl: '../views/configuracionPlanilla-update.component.html',
	providers: [PeriodoService, ConfiguracionPlanillaService]
})

export class ConfiguracionPlanillaUpdate{
	public configuracionPlanilla: ConfiguracionPlanilla;
	public periodo: Periodo;
	public listaPeriodo: Periodo[];

	constructor(
		private _configuracionPlanillaService: ConfiguracionPlanillaService,
		private _periodoService: PeriodoService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){

	}

	ngOnInit(){
		this.getConfiguracionPlanilla();
		this.getPeriodos();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
	}

	getPeriodos(){
		this._periodoService.getPeriodos().subscribe(
			response =>{
				this.listaPeriodo = response;

			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	getConfiguracionPlanilla(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._configuracionPlanillaService.getConfiguracionPlanilla(id).subscribe(
				response =>{
					this.configuracionPlanilla = response;	
					console.log(this.configuracionPlanilla);
				},
				error => {
					console.log(<any>error);
				})

		});
	}


	onSubmit(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._configuracionPlanillaService.updateConfiguracionPlanilla(id, this.configuracionPlanilla).subscribe(
			response=>{

				this.alertService.success({
	        			title: 'La nueva configuracion fue editada exitosamente!'
	      			});
					this._router.navigate(['/configuracionPlanilla']);

			},
			error => {
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});


	}
			);
	
		});

	}
}