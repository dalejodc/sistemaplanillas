import { Component } from '@angular/core';
import { CentroDeCostos } from '../models/centro_de_costos'
import { CentroDeCostosService } from '../services/centro_de_costos.service'

@Component({
  selector: 'chartCostos',
  templateUrl: '../views/chart.component.html'
})
export class Chart {
  public listCentros: CentroDeCostos[];
  public costos: number[];
  public nombres: string[];
  constructor(
    private _centroCService: CentroDeCostosService

    ){
    this.nombres = [];
    this.costos = [];

    this.getCentrosCostos();

  }

  // lineChart
  public lineChartData: Array<any> = [
  {data: [65, 59, 80, 81, 56, 55, 40], label: 'Costos por departamento'}
  ];

  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(0, 218, 23,0.5)',
      borderColor: 'rgba(26 ,208,111,,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public barChartType = 'bar';

  public randomize(): void {
    const _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  getCentrosCostos(){
    this._centroCService.getCentroCostos().subscribe(
      response =>{
        this.listCentros = response;
        console.log(this.listCentros);
        for (let i = 0; i < this.listCentros.length; i++)
        {
          this.costos[i] = Number(this.listCentros[i].centro_costo_presupuesto);
          this.nombres[i] = this.listCentros[i].nivelEstructura.nivel_estructura_nombre;
        }
        
        // let clone = JSON.parse(JSON.stringify(this.lineChartData));
        // clone[1].data = this.costos;
        // this.lineChartData = clone;

        console.log(this.costos);
          this.lineChartData[0].data = this.costos;
          this.lineChartLabels =this.nombres;


      },
      error =>{
        console.log(error);
      }
      );
  }
}