
import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { Empleado } from '../models/empleado'
import { EmpladoService } from '../services/empleado.service'
import { Router, ActivatedRoute, Params } from '@angular/router'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'empleado',
	templateUrl: '../views/empleado.component.html',
	providers: [EmpladoService]
})

export class GetEmpleado{
	public empleado: Empleado;
	public now: Date = new Date();
	public edad: number;
	public fechaN: Date;

	constructor(
		private _empleadoService: EmpladoService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	


		){

		setInterval(() => {this.now = new Date();}, 1);

	}

	ngOnInit(){
		this.getEmpleado();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getEmpleado(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._empleadoService.getEmpleado(id).subscribe(
			response =>{
				this.empleado = response;
				console.log(this.empleado);
				this.fechaN = new Date (this.empleado.persona.persona_fecha_nacimiento);
				console.log(this.fechaN);
				console.log(this.now);
				this.edad = Number(this.now.getFullYear()) - Number(this.fechaN.getFullYear());
				console.log(this.edad);


			},
			error =>{
				console.log(<any>error);
			}
			);
	
	});

}

}
