import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Subject } from 'rxjs/Rx';

declare var $:any;
declare var Materialize:any;

import { RentaService } from '../services/renta.service';
import { Renta } from '../models/renta';

@Component({
	selector: 'renta',
	templateUrl: '../views/renta-list.component.html',
	providers: [RentaService]
})

export class RentaComponent {

	public listaRenta : Renta[];

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _rentaService: RentaService,
		private alertService: SweetAlertService	
	){
		this.dtOptions = {
		    pageLength: 4,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getRentas();
	}

	getRentas(){
		this._rentaService.getRentas().subscribe(
			result => {
				this.listaRenta = result;
				console.log(this.listaRenta);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}