import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { RentaService } from '../services/renta.service';
import { Renta } from '../models/renta';
import { PeriodoService } from '../services/periodo.service';
import { Periodo } from '../models/periodo';


@Component({
	selector: 'genero-edit',
	templateUrl: '../views/renta-add.component.html',
	providers: [RentaService, PeriodoService]
})

export class RentaEditComponent{
	public renta : Renta;
	public listaPeriodos: Periodo[];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _rentaService: RentaService,
		private _periodoService: PeriodoService,
		private alertService: SweetAlertService	
	){
		this.renta = new Renta(null, null, null, null,null, null, null);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getRenta();
		this.getPeriodos();
	}

	getRenta(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._rentaService.getRenta(id).subscribe(
				response=>{
						this.renta= response;
						console.log(this.renta);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	getPeriodos(){
		this._periodoService.getPeriodos().subscribe(
			result =>{
				this.listaPeriodos = result;
				console.log(this.listaPeriodos);
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}

	guardarRenta(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._rentaService.editRenta(id, this.renta).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡La renta ' + this.renta.renta_nombre+ ' fue actualizada exitosamente!'
	      			});
					this._router.navigate(['/renta']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}