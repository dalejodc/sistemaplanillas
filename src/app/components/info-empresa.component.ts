import { Component } from '@angular/core';
declare var $:any;
declare var Materialize:any;

import { InfoEmpresaService } from '../services/info-empresa.service';
import { InfoEmpresa } from '../models/info_empresa';
import { ConfiguracionEmpresa } from '../models/configuracion_empresa';

import { Plan } from '../models/plan'
import { Estado } from '../models/estado_empresa'

@Component({
	selector: 'info-empresa',
	templateUrl: '../views/info-empresa.component.html',
	providers: [InfoEmpresaService]
})

export class InfoEmpresaComponent {

	public emp : InfoEmpresa;
	public empresa : ConfiguracionEmpresa;
	public plan : Plan;
	public estado: Estado;
	public mostrar = false;

	constructor(
		private _infoEmpresaService: InfoEmpresaService
	){ 
		this.empresa = new ConfiguracionEmpresa (null, null, this.estado, null, this.plan, null, null, null, null, null, null, null, null);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		// this.getInfoEmpresa();
		this.getEmpresa();
	}

	getInfoEmpresa(){
		this._infoEmpresaService.getInfoEmpresa().subscribe(
			result => {
				this.plan = result.plan;
				this.emp = result;
				console.log(this.emp);
			},
			error => {
				console.log(<any>error);
			}
			);
	}
	getEmpresa(){
		this._infoEmpresaService.getEmpresa().subscribe(
			result => {
				this.empresa = result;
				console.log(result);
				this.mostrar = true;
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}