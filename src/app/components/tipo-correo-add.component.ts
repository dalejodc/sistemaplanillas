import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { TipoCorreoService } from '../services/tipo-correo.service';
import { TipoCorreo } from '../models/tipo_correo';

@Component({
	selector: 'tipo-correo-add',
	templateUrl: '../views/tipo-correo-add.component.html',
	providers: [TipoCorreoService]
})

export class TipoCorreoAddComponent {

	public tipoCorreo: TipoCorreo;

	constructor(
		private _tipoCorreoService: TipoCorreoService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.tipoCorreo= new TipoCorreo (null, null, null);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
	}

	guardarCorreo(){
		console.log(this.tipoCorreo)
		this._tipoCorreoService.addCorreo(this.tipoCorreo).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El tipo de correo ' + this.tipoCorreo.tipo_correo_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/tipo-correo']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}