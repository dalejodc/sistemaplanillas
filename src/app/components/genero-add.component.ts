import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { GeneroService } from '../services/genero.service';
import { Genero } from '../models/genero';

@Component({
	selector: 'genero',
	templateUrl: '../views/genero-add.component.html',
	providers: [GeneroService]
})

export class GeneroAddComponent {

	public genero: Genero;

	constructor(
		private _generoService: GeneroService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.genero= new Genero ( null, null, null, true);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
	}

	guardarGenero(){
		console.log(this.genero);

		this._generoService.addGenero(this.genero).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El género ' + this.genero.genero_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/genero']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}