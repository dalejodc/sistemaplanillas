import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ConfiguracionPlanilla } from '../models/configuracion_planilla'
import { Genero } from '../models/genero'
import { ConfiguracionPlanillaService } from '../services/configuracion_planilla.service'
import { GeneroService } from '../services/genero.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'configuracionPlanilla',
	templateUrl: '../views/configuracionPlanilla-list.component.html',
	providers: [ConfiguracionPlanillaService,GeneroService]
})

export class ConfiguracionPlanillaComponent{
	public listaConfigPlanilla : ConfiguracionPlanilla;
	public listaGenerosEmpresa : Genero[];
	public listaGeneros : Genero[];
	public agregarG : boolean;
	public genero: Genero;

	constructor(
		private _configuracionPlanillaService: ConfiguracionPlanillaService,
		private _generoService: GeneroService,
		private _router: Router,
		private alertService: SweetAlertService

		){
		
	}

	ngOnInit(){
		this.getConfiguracionPlanillas();
		this.getGenero();
		this.agregarG=false;
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getConfiguracionPlanillas(){
		this._configuracionPlanillaService.getConfiguracionPlanillas().subscribe(
			response =>{
				this.listaConfigPlanilla = response;
				console.log(this.listaConfigPlanilla);

			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	getGenero(){
		console.log('hola');
		this._generoService.getGeneros().subscribe(
			response=>{
				this.listaGenerosEmpresa=response;
				console.log(this.listaGenerosEmpresa);
			},
			error=>{
				console.log(<any>error);
			}
		);
	}

	//LLena el catalogo de generos disponibles en el sistema informatico

	agregarGenero(){
		this._generoService.getGenerosEmpresa().subscribe(
			response=>{
				this.listaGeneros=response;
				console.log(this.listaGeneros);
				this.agregarG=true;
			},
			error=>{
				console.log(<any>error);
			}
		);
	}
	//Guardar un genero
	guardarGenero(){
	console.log(this.genero);
	this._generoService.addGenero(this.genero).subscribe(
		response =>{
			this.alertService.success({
							title: '¡El género ' + this.genero.genero_nombre+ ' fue guardado exitosamente!'
						});
			this.getGenero();
			this._router.navigate(['/configuracionPlanilla']);
		},
		error =>{
			this.alertService.error({
							title: '¡Ocurrió un error!'
					});
			console.log(<any>error);
		}
	);
}


	}
