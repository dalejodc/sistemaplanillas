import { Component } from '@angular/core'
import { Comision } from '../models/comision'
import { ComisionService} from '../services/comision.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'comision-add',
	templateUrl : '../views/comision-add.component.html',
	providers: [ComisionService]
})

export class ComisionAdd{
	public t : Comision;

	constructor(
		private _comisionService: ComisionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.t = new Comision(0,null,null,null,null)

	}

	ngOnInit(){
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();

	}

	onSubmit(){

		if (this.t.comision_minimo<this.t.comision_maximo){

		this.t.comision_porcentaje=this.t.comision_porcentaje/100;
		
		this._comisionService.addComision(this.t).subscribe(

				response =>{

					this.alertService.success({
	        			title: '¡El tipo de transacción ' + this.t.comision_id+ ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/comision']);
				},

				error =>{
					console.log(error);
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				}

			);

	}
	else{

		this.alertService.error({
	        			title: '¡El mínimo y el máximo no son válidos !'});

	}
	}

}