
import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { CentroDeCostos } from '../models/centro_de_costos'
import { CentroDeCostosService } from '../services/centro_de_costos.service'
import { Router, ActivatedRoute, Params } from '@angular/router'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'centroDeCostoGet',
	templateUrl: '../views/centroDeCosto.component.html',
	providers: [CentroDeCostosService]
})

export class GetCentroCosto{
	public centroC: any;
	

	constructor(
		private _centroService: CentroDeCostosService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	


		){


	}

	ngOnInit(){
		this.getCentroCosto();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getCentroCosto(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._centroService.getCentroCosto(id).subscribe(
			response =>{
				this.centroC = response;
				console.log(this.centroC);
			

			},
			error =>{
				console.log(error);
			}
			);
	
	});

}

}
