import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Subject } from 'rxjs/Rx';

import { Empleado } from '../models/empleado'
import { EmpladoService } from '../services/empleado.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'empleado',
	templateUrl: '../views/empleado-usuario-list.component.html',
	providers: [EmpladoService]
})

export class EmpleadoUsuarioComponent{

	public listEmpleados: Empleado[];

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _empleadoService: EmpladoService,
		private alertService: SweetAlertService	
		){
		this.dtOptions = {
		    pageLength: 8,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}

	ngOnInit(){
		this.getEmpleados();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
	}

	getEmpleados(){
		this._empleadoService.getEmpleados().subscribe(
			response =>{
				this.listEmpleados = response;
				console.log(this.listEmpleados);
				this.dtTrigger.next();
			},
			error =>{
				console.log(<any>error);
			}
			);
	}

}
