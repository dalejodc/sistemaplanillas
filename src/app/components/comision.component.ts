import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { Comision } from '../models/comision'
import { ComisionService } from '../services/comision.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'comision',
	templateUrl: '../views/comision-list.component.html',
	providers: [ComisionService]
})

export class ComisionComponent{
	public listComisiones: Comision[];

	constructor(
		private _comisionService: ComisionService,
		private alertService: SweetAlertService	


		){

	}

	ngOnInit(){
		this.getComisiones();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getComisiones(){
		this._comisionService.getComisiones().subscribe(
			response =>{
				this.listComisiones = response;
				console.log(this.listComisiones);

			},
			error =>{
				console.log(<any>error);
			}
			);
	}


	onDelete(id, comision: Comision){
		

		this.alertService.confirm({
	      title: '¿Desea deshabilitar la comisión: '+comision.comision_id+'?',
	      text: "La Comisión se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
	    	this._comisionService.deleteComision(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡La comisión ' + comision.comision_nombre + ' fue deshabilitada exitosamente!'
					});
					this.getComisiones();

				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text:  comision.comision_nombre+", "+ error.statusText
	      			});
					console.log(<any>error)
					this.getComisiones();
				}


			);

	    })
	    .catch(() => console.log('canceled'));		
	}
}