import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { PlanService } from '../services/plan.service';
import { Plan } from '../models/plan';

@Component({
	selector: 'plan-edit',
	templateUrl: '../views/plan-add.component.html',
	providers: [PlanService]
})

export class PlanEditComponent{
	public plan :Plan;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _planService: PlanService,
		private alertService: SweetAlertService	
	){
		this.plan = new Plan(null,null, null, null);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getPlan();
	}

	getPlan(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			console.log(id);

			this._planService.getPlan(id).subscribe(
				response=>{
						this.plan= response;
						console.log(this.plan);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarPlan(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._planService.editPlan(id, this.plan).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El plan ' + this.plan.plan_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/planes']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}