import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { SweetAlertService } from 'angular-sweetalert-service';


declare var $:any;
declare var Materialize:any;

import { UsuarioService } from '../services/usuario.service';

@Component({
	selector: 'mi-usuario',
	templateUrl: '../views/mi-usuario.component.html',
	providers: [UsuarioService]
})

export class MiUsuarioComponent implements OnInit {

	public nuevo : boolean;
	public mostrar : boolean;
	public usuario_nombre: string
	public usuario_pass: string
	public usuario_actual_pass: string
	public usuario_pass2: string
	public usuario_rol: string
	public usuario_ultimo_intento: Date;

	constructor(
		private _usuarioService: UsuarioService,
		private alertService: SweetAlertService	
	){
		this.nuevo = false;
		this.mostrar = true;
		$(document).ready(function() { Materialize.updateTextFields(); });
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getUsuario();
	}

	getUsuario(){

		this._usuarioService.getUsuario().subscribe(
			data => {
				// this.listaGeneros = result;
				console.log(data);
				this.usuario_nombre = data.usuario_nombre;
				this.usuario_rol = sessionStorage.getItem('rol');
				this.usuario_ultimo_intento = data.usuario_ultimo_intento;

				if(data.usuario_es_nuevo ==true){
					this.nuevo = true;
				}
			},
			error => {
				alert('error');
				this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text: error
	      			});
				// console.log(<any>error);
			}
		);
	}

	editar(){
		this.mostrar = false;
		$(document).ready(function() { Materialize.updateTextFields(); });
	}

	cancelar(){
		this.mostrar = true;
		this.usuario_pass = null;
		this.usuario_pass2 = null;
	}

	guardar(){
		/*
		{"usuario_nombre":"carlos Mata","contraseniaAnterior":"Admin123","usuario_contrasenia":"Admin123","rol_id":2,"usuario_estado":"true"}
		*/
		interface USER {
			    usuario_nombre: string,
			    contraseniaAnterior : string,
			    usuario_contrasenia: string,
			    usuario_estado: boolean
			};

		//Creando objeto auxiliar para que sea aceptado en el formato de la URL
		const us : USER = {
				usuario_nombre: this.usuario_nombre,
			    contraseniaAnterior : this.usuario_actual_pass,
			    usuario_contrasenia: this.usuario_pass,
			    usuario_estado: true
		};

		console.log(us);

		if( (this.usuario_pass != this.usuario_pass2) || this.usuario_pass == null){
			Materialize.toast('Contraseñas no coinciden', 2000);
			// this.usuario_pass = null;
			// this.usuario_pass2 = null;
		}else{
			this._usuarioService.editUsuario(us).subscribe(
				response => {
					this.alertService.success({
	        			title: '¡Su usuario fue actualizado exitosamente!'
	      			});
					this.mostrar = true;
					sessionStorage.removeItem('user');
					sessionStorage.setItem('user', this.usuario_nombre);
				},
				error => {
					this.alertService.error({
	        			title: '¡'+ <any>error +'!'
	      			});
					console.log(<any>error);
				}
			);
		}
	}
}