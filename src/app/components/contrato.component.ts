import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { SweetAlertService } from 'angular-sweetalert-service';


declare var $:any;
declare var Materialize:any;

import { ContratoService } from '../services/contrato.service';
import { Contrato } from '../models/contrato';

@Component({
	selector: 'genero',
	templateUrl: '../views/contrato-list.component.html',
	providers: [ContratoService]
})

export class ContratoComponent implements OnInit {

	public listaContrato : Contrato[];
	public contrato: Contrato;

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _contratoService: ContratoService,
		private alertService: SweetAlertService	
	){

		 // opciones de DataTable
		  this.dtOptions = {
		    pageLength: 10,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getContratos();
	}

	getContratos(){
		this._contratoService.getContratos().subscribe(
			result => {
				this.listaContrato = result;
				console.log(this.listaContrato);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	// deshabilitar(genero){
	// 	this.alertService.confirm({
	//       title: '¿Desea deshabilitar el género '+genero.genero_nombre+'?',
	//       text: "El género se deshabilitará y no podrá asignarse a los empleados.",
	//       cancelButtonColor: '#E57373',
 //  		  confirmButtonColor: '#83C283',
 //  		  confirmButtonText: 'Aceptar'
	//     })
	//     .then(() => {
	// 	    this.genero = genero;
	// 		this.genero.genero_estado =false;

	// 		this._generoService.editGenero(genero.genero_id, this.genero).subscribe(
	// 			response =>{
	// 				this.alertService.success({
	//         			title: '¡El género ' + this.genero.genero_nombre+ ' fue deshabilitado exitosamente!'
	//       			});
	// 				this.ngOnInit();
	// 			},
	// 			error =>{
	// 				this.alertService.error({
	//         			title: '¡Ocurrió un error!'
	//       			});
	// 				console.log(<any>error);
	// 			}
	// 		);	
	//     })
	//     .catch(() => console.log('canceled'));
	// }
}