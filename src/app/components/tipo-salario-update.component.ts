import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { TipoSalario } from '../models/tipo_salario'
import { ConfiguracionEmpresa } from '../models/configuracion_empresa';
//import { Empresa } from '../models/empresa'
import { TipoSalarioService } from '../services/tipo-salario.service'
import { EmpresaService } from '../services/empresa.service';

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'tipoSalario',
	templateUrl: '../views/tipoSalario-update.component.html',
	providers: [EmpresaService, TipoSalarioService]
})

export class TipoSalarioUpdate{
	public tipoSalario: TipoSalario;

	public listaEmpresas : ConfiguracionEmpresa[];
	public empresa: ConfiguracionEmpresa;


	constructor(
		private _tipoSalarioService: TipoSalarioService,
		private _empresaService: EmpresaService,
		
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){
	}

	ngOnInit(){
		this.getTipoSalario();
		this.getEmpresas();

		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
	
	}

	
	getEmpresas(){
		this._empresaService.getEmpresas().subscribe(
			result => {
				this.listaEmpresas = result;
				console.log(this.listaEmpresas);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	

	getTipoSalario(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._tipoSalarioService.getTipoSalario(id).subscribe(
				response =>{
					this.tipoSalario = response;	
					console.log(this.tipoSalario);
				},
				error => {
					console.log(<any>error);
				})

		});
	}


	onSubmit(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._tipoSalarioService.updateTipoSalario(id, this.tipoSalario).subscribe(
			response=>{

				this.alertService.success({
	        			title: '¡El tipo de Salario ' + this.tipoSalario.tipo_salario_nombre+ ' fue editado exitosamente!'
	      			});
					this._router.navigate(['/tipoSalario']);

			},
			error => {
				this.alertService.error({
	        			title: '¡Ocurrió un error!'});
				console.log(<any>error);});
	
		});

	}


}