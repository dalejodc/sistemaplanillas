import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { GeneroService } from '../services/genero.service';
import { Genero } from '../models/genero';

@Component({
	selector: 'genero-edit',
	templateUrl: '../views/genero-add.component.html',
	providers: [GeneroService]
})

export class GeneroEditComponent{
	public genero : Genero;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _generoService: GeneroService,
		private alertService: SweetAlertService	
	){
		this.genero = new Genero(null,null, null, true);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getGenero();
	}

	getGenero(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			console.log(id);

			this._generoService.getGenero(id).subscribe(
				response=>{
						this.genero= response;
						console.log(this.genero);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarGenero(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._generoService.editGenero(id, this.genero).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El género ' + this.genero.genero_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/genero']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}