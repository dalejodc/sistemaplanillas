import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { TipoSalario } from '../models/tipo_salario'
import { Empresa } from '../models/empresa'
import { TipoSalarioService } from '../services/tipo-salario.service'
import { EmpresaService } from '../services/empresa.service';

declare var $:any;
declare var Materialize:any;



@Component({
	selector: 'tipoSalario-add',
	templateUrl: '../views/tipoSalario-add.component.html',
	providers: [EmpresaService, TipoSalarioService]
})

export class TipoSalarioAdd{
	public tipoSalario: TipoSalario;
	
	//public listaEmpresas : Empresa[];
	public empresa: Empresa;

	constructor(
		
		private _tipoSalarioService: TipoSalarioService,
	
		private _empresaService: EmpresaService,

		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){
		//this.empresa= new Empresa(null, 3, null, null, null, null,null,null, null, null);
		
		this.tipoSalario = new TipoSalario(0, '','');
 
	}

	ngOnInit(){
		
		this.getEmpresas();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	
	getEmpresas(){
		this._empresaService.getEmpresas().subscribe(
			result => {
				this.empresa = result;
				console.log(this.empresa);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	onSubmit(){
		
		this._tipoSalarioService.addTipoSalario(this.tipoSalario).subscribe(
			response=>{

				this.alertService.success({
	        			title: '¡El tipo de Salario ' + this.tipoSalario.tipo_salario_nombre+ ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/tipoSalario']);

			},
			error => {
				console.log(<any>error);
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});


			}
			);
}
}