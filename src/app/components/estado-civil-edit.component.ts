import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { EstadoCivilService } from '../services/estado-civil.service';
import { EstadoCivil } from '../models/estado_civil';

@Component({
	selector: 'estado-civil-edit',
	templateUrl: '../views/estado-civil-add.component.html',
	providers: [EstadoCivilService]
})

export class EstadoCivilEditComponent{
	public estadoCivil : EstadoCivil;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _estadoCivilService: EstadoCivilService,
		private alertService: SweetAlertService	
	){
		this.estadoCivil = new EstadoCivil(null,null, null, true);
	}

	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		$(document).ready(function() { Materialize.updateTextFields(); });
		this.getEstadoCivil();
	}

	getEstadoCivil(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			console.log(id);

			this._estadoCivilService.getEstadoCivil(id).subscribe(
				response=>{
						this.estadoCivil= response;
						console.log(this.estadoCivil);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarEstadoCivil(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._estadoCivilService.editEstadoCivil(id, this.estadoCivil).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El estado civil ' + this.estadoCivil.estado_civil_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/estado-civil']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}