import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { TipoTelefonoService } from '../services/tipo-telefono.service';
import { TipoTelefono } from '../models/tipo_telefono';

@Component({
	selector: 'tipo-telefono-add',
	templateUrl: '../views/tipo-telefono-add.component.html',
	providers: [TipoTelefonoService]
})

export class TipoTelefonoAddComponent {

	public tipoTelefono: TipoTelefono;

	constructor(
		private _tipoTelefonoService: TipoTelefonoService,
		private _router: Router,
		private alertService: SweetAlertService
	){
	this.tipoTelefono= new TipoTelefono (null, null, null);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
	}

	guardarTelefono(){
		console.log(this.tipoTelefono);
		
		this._tipoTelefonoService.addTelefono(this.tipoTelefono).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El tipo de telefono ' + this.tipoTelefono.tipo_telefono_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/tipo-telefono']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}