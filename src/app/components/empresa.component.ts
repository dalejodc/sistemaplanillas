import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { EmpresaService } from '../services/empresa.service';
import { ConfiguracionEmpresa } from '../models/configuracion_empresa';

@Component({
	selector: 'empresa',
	templateUrl: '../views/empresa-list.component.html',
	providers: [EmpresaService]
})

export class EmpresaListComponent {

	public listaEmpresas : ConfiguracionEmpresa[];
	public listaDeshabilitadas : ConfiguracionEmpresa[];
	public listaPendientes : ConfiguracionEmpresa[];
	public empresa: ConfiguracionEmpresa;

	constructor(
		private _empresaService: EmpresaService,
		private alertService: SweetAlertService	
	){}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getEmpresas();
		this.getEmpresasDeshabilitadas();
		this.getEmpresasPendientes();
	}

	getEmpresas(){
		this._empresaService.getEmpresas().subscribe(
			result => {
				this.listaEmpresas = result;
				console.log(this.listaEmpresas);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getEmpresasDeshabilitadas(){
		this._empresaService.getEmpresasDeshabilitadas().subscribe(
			result => {
				this.listaDeshabilitadas = result;
				console.log("Deshabilitadas", this.listaDeshabilitadas);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getEmpresasPendientes(){
		this._empresaService.getEmpresasPendientes().subscribe(
			result => {
				this.listaPendientes = result;
				console.log(this.listaPendientes);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	deshabilitar(empresa){
		this.alertService.confirm({
	      title: '¿Desea deshabilitar la empresa '+empresa.empresa_nombre+'?',
	      text: "La empresa se deshabilitará",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
		    this.empresa = empresa;
			this.empresa.estado_id =2; 

			this._empresaService.editEmpresa(empresa.empresa_id, this.empresa).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡La empresa ' + this.empresa.empresa_nombre+ ' fue deshabilitada exitosamente!'
	      			});
					this.ngOnInit();
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);	
	    })
	    .catch(() => console.log('canceled'));
	}

	habilitar(empresa){

		this.empresa = empresa;
			this.empresa.estado_id =1; 

			//Objeto Auxiliar para poder complir con el formato requerido en empresa/solicitud/:id
			interface EMP {
			    empresa_id: number,
			    estado_id : number
			};

			const emp : EMP = {empresa_id : this.empresa.empresa_id, estado_id: this.empresa.estado_id};
			console.log(emp);
			console.log(JSON.stringify(emp));

		this.alertService.confirm({
	      title: '¿Desea habilitar la empresa '+empresa.empresa_nombre+'?',
	      text: "La empresa se habilitará",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {

	    	this._empresaService.enviarCorreo(this.empresa.empresa_id, emp).subscribe(
	    		response => {
	    			this.alertService.success({
	        			title: '¡La empresa ' + this.empresa.empresa_nombre+ ' fue habilitada exitosamente!',
	        			text: 'Se ha enviado un correo a ' +this.empresa.empresa_correo_representante,
	      			});
					this.ngOnInit();
	    		},
	    		error => {
	    			this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
	    		}
	    		)
			
			// this._empresaService.editEmpresa(empresa.empresa_id, this.empresa).subscribe(
			// 	response =>{
			// 		this.alertService.success({
	  //       			title: '¡La empresa ' + this.empresa.empresa_nombre+ ' fue habilitada exitosamente!',
	  //       			text: 'Se ha enviado un correo a ' +this.empresa.empresa_correo_representante,
	  //     			});
	  //     			this._empresaService.enviarCorreo(this.empresa.empresa_id, emp);
			// 		this.ngOnInit();
			// 	},
			// 	error =>{
			// 		this.alertService.error({
	  //       			title: '¡Ocurrió un error!'
	  //     			});
			// 		console.log(<any>error);
			// 	}
			// );	
	    })
	    .catch(() => console.log('canceled'));
	}
}