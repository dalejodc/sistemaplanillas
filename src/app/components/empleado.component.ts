import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Subject } from 'rxjs/Rx';

import { Empleado } from '../models/empleado'
import { EmpladoService } from '../services/empleado.service'
import { Router, ActivatedRoute, Params } from '@angular/router'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'empleado',
	templateUrl: '../views/empleado-list.component.html',
	providers: [EmpladoService]
})

export class EmpleadoComponent{

	public listEmpleados: Empleado[];

	dtOptions: any = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _empleadoService: EmpladoService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	
		){
		this.dtOptions = {
		    pageLength: 8,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    },
		    buttons: [
		        'excel',
		        'pdf'
		      ]
		  };
	}

	ngOnInit(){
		this.getEmpleados();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

		// $('#example').DataTable( {
		//     buttons: [
		//         'pdf'
		//     ]
		// } );

	}

	getEmpleados(){
		this._empleadoService.getEmpleados().subscribe(
			response =>{
				this.listEmpleados = response;
				console.log(this.listEmpleados);
				this.dtTrigger.next();
			},
			error =>{
				console.log(<any>error);
			}
			);
	}


	onDelete(id, empleado){
		

		this.alertService.confirm({
	      title: '¿Desea deshabilitar el empleado: '+empleado.persona.persona_primer_nombre + ' '+empleado.persona.persona_primer_apellido +'?',
	      text: "La Transacción se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {

	    	this._empleadoService.deleteEmpleado(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡El Empleado' + empleado.persona.persona_primer_nombre + ' '+empleado.persona.persona_primer_apellido  + ' fue deshabilitado exitosamente!'
					});
					this.getEmpleados();

				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text:  error.statusText
	      			});
					console.log(error);
					this.getEmpleados();
					
				}


			);

	    })

	    .catch(() => console.log('canceled'));		
	}
}
