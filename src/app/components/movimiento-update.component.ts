import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { Movimiento } from '../models/movimiento'
import { TipoTransaccion } from '../models/tipo_transaccion'

import { MovimientoService } from '../services/movimiento.service'
import { TipoTransaccionService } from '../services/tipo_transaccion.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'movimiento',
	templateUrl: '../views/movimiento-update.component.html',
	providers: [TipoTransaccionService, MovimientoService]
})

export class MovimientoUpdate{
	public movimiento: Movimiento;
	public tipoTransaccion: TipoTransaccion;
	public listaTipoTransaccion: TipoTransaccion[];

	constructor(
		private _movimientoService: MovimientoService,
		private _tipoTransaccionService: TipoTransaccionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){


	}

	ngOnInit(){
		this.getMovimiento();
		this.getTipoTransacciones();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
		

	}

	getTipoTransacciones(){
		this._tipoTransaccionService.getTipoTransacciones().subscribe(
			response =>{
				this.listaTipoTransaccion = response;

			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	getMovimiento(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._movimientoService.getMovimiento(id).subscribe(
				response =>{
					this.movimiento = response;	
				},
				error => {
				})

		});
	}


	onSubmit(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._movimientoService.updateMovimiento(id, this.movimiento).subscribe(
			response=>{

				this.alertService.success({
	        			title: '¡El movimiento ' + this.movimiento.movimiento_nombre+ ' fue editado exitosamente!'
	      			});
					this._router.navigate(['/movimiento']);

			},
			error => {
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});


	}
			);
	
		});

	}
}