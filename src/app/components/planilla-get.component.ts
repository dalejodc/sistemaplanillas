import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { Planilla } from '../models/planilla'
import { PlanillaService } from '../services/planilla.service'
import { EmpresaService } from '../services/empresa.service'
import { InfoEmpresaService } from '../services/info-empresa.service'
import { DetalleMovimientoService } from '../services/detalleMovimiento.service'
import { DetalleMovimiento } from '../models/detalleMovimiento'

import { GlobalConfig } from  '../services/global-config'

import { Empresa } from '../models/empresa'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'planilla-get',
	templateUrl: '../views/planilla-get.component.html',
	providers: [PlanillaService,EmpresaService, InfoEmpresaService, DetalleMovimientoService]
})

export class PlanillaGetComponent{
	public planilla: Planilla;
	public empresa: Empresa;
	public totalI: number;
	public totalD: number;
	public totalO: number;
	public totalP: number;
	

	constructor(
		private _planillaService: PlanillaService,
		private _empresaService: EmpresaService,
		private _detalleMovimientoService: DetalleMovimientoService,
		private _infoEmpresaService: InfoEmpresaService,
		private alertService: SweetAlertService,
		private _route: ActivatedRoute,
		private _router: Router


		){
	

	}

	ngOnInit(){

		this.totalI = 0.0;
		this.totalD = 0.0;
		this.totalO = 0.0;

		

		this.getPlanilla();
		this.getEmpresa();


		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
		
	

	}

	getPlanilla(){
	this._route.params.forEach((params: Params)=>{
		let id = params['id'];

		this._planillaService.getPlanilla(id).subscribe(
			response =>{
				this.planilla = response;	
				console.log(this.planilla);
				GlobalConfig.planillaId = this.planilla.planilla_id;
				console.log(GlobalConfig.planillaId);



					for (let i = 0; i < this.planilla.detalleMovimiento.length; i++) {

					if (this.planilla.detalleMovimiento[i].movimiento.tipoTransaccion.tipo_transaccion_nombre == "Ingreso"){
						
						this.totalI +=  Number(this.planilla.detalleMovimiento[i].detalle_movimiento_monto);
					}
					else{

						if (this.planilla.detalleMovimiento[i].movimiento.tipoTransaccion.tipo_transaccion_nombre == "Descuento"){
							
								this.totalD += Number(this.planilla.detalleMovimiento[i].detalle_movimiento_monto);
						}
					else{

						if(this.planilla.detalleMovimiento[i].movimiento.tipoTransaccion.tipo_transaccion_signo == "-"){

							this.totalO += Number(this.planilla.detalleMovimiento[i].detalle_movimiento_monto) * -1;
						}
						else{
							this.totalO += Number(this.planilla.detalleMovimiento[i].detalle_movimiento_monto);


						}
						
					}
					}
		            }

		     	this.totalP = Number(this.planilla.empleado.salario.salario_monto_base) + this.totalI - this.totalD + this.totalO
		     	+ Number(this.planilla.planilla_comision)- Number(this.planilla.planilla_renta);
			},
			error => {
			})

	});
	}

	getEmpresa(){
		this._infoEmpresaService.getEmpresa().subscribe(
			result => {
				this.empresa = result;
				console.log(this.empresa);
				
			},
			error => {
				console.log(<any>error);
			}
			);
		}
		

	onDelete(id, detalleMovimietno: DetalleMovimiento){
		console.log(id);
		this.alertService.confirm({
	      title: '¿Desea deshabilitar El movimiento '+detalleMovimietno.movimiento.movimiento_nombre+'?',
	      text: "La Comisión se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
	    	this._detalleMovimientoService.deleteDetalleMovimiento(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡El movimiento ' + detalleMovimietno.movimiento.movimiento_nombre+ ' fue eliminado exitosamente!'
					});
					this.ngOnInit();

				},
				error => {

	      			this.ngOnInit();
					this.alertService.success({
					title: '¡El movimiento ' + detalleMovimietno.movimiento.movimiento_nombre+ ' fue eliminado exitosamente!'
					});
					console.log(<any>error);
				}


			);

	    })
	    .catch(() => console.log('canceled'));		
	}



}	


	

