import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { CategoriaSalario } from '../models/categoria_salario'
import { Puesto } from '../models/puesto'

import { ConfiguracionEmpresa } from '../models/configuracion_empresa';
//import { Empresa } from '../models/empresa'

import { CategoriaSalarioService } from '../services/categoria_salario.service'
import { PuestoService } from '../services/puesto.service'

import { EmpresaService } from '../services/empresa.service';

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'categoriaSalario',
	templateUrl: '../views/categoriaSalario-update.component.html',
	providers: [PuestoService,EmpresaService, CategoriaSalarioService]
})

export class CategoriaSalarioUpdate{
	public categoriaSalario: CategoriaSalario;
	public puesto: Puesto;
	public listaPuestos: Puesto[];
	public listaEmpresas : ConfiguracionEmpresa[];
	public empresa: ConfiguracionEmpresa;


	constructor(
		private _categoriaSalarioService: CategoriaSalarioService,
		private _puestoService: PuestoService,
		private _empresaService: EmpresaService,
		
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){


	}

	ngOnInit(){
		this.getCategoriaSalario();
		this.getPuestos();
		this.getEmpresas();

		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
		

	}

	getPuestos(){
		this._puestoService.getPuestos().subscribe(
			response =>{
				this.listaPuestos = response;

			},
			error=>{
				console.log(<any>error);
			}

		);
	}
	getEmpresas(){
		this._empresaService.getEmpresas().subscribe(
			result => {
				this.listaEmpresas = result;
				console.log(this.listaEmpresas);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	

	getCategoriaSalario(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._categoriaSalarioService.getCategoriaSalario(id).subscribe(
				response =>{
					this.categoriaSalario = response;	
					console.log(this.categoriaSalario);
				},
				error => {
				})

		});
	}


	onSubmit(){

		if (this.categoriaSalario.categoria_salario_minimo<this.categoriaSalario.categoria_salario_maximo){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._categoriaSalarioService.updateCategoriaSalario(id, this.categoriaSalario).subscribe(
			response=>{

				this.alertService.success({
	        			title: '¡La Categoria de Salario ' + this.categoriaSalario.categoria_salario_nombre+ ' fue editada exitosamente!'
	      			});
					this._router.navigate(['/categoriaSalario']);

			},
			error => {
				this.alertService.error({
	        			title: '¡Ocurrió un error!'});


	}
	);
	
		});
	}	

else{


this.alertService.error({title: '¡El mínimo y el máximo no son válidos !'});

	}

	}
}