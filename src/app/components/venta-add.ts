import { Component } from '@angular/core'
import { Venta } from '../models/venta'
import { PlanillaService} from '../services/planilla.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'

import { GlobalConfig } from  '../services/global-config'

declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'venta-add',
	templateUrl : '../views/venta-add.component.html',
	providers: [PlanillaService]
})

export class VentaAdd{
	public venta : Venta;

	constructor(
		private _ventaService: PlanillaService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.venta = new Venta(GlobalConfig.planillaId, null);

	}

	ngOnInit(){

		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();
		console.log(GlobalConfig.planillaId);

	}

	onSubmit(){
		console.log(this.venta);
		this._ventaService.regVenta(this.venta).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡La venta fue registrada fue guardado exitosamente!'
	      			});
					this._router.navigate(['/planilla/' + GlobalConfig.planillaId]);
				},

				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
					this._router.navigate(['/planilla/' + GlobalConfig.planillaId]);
				}

			);

	}

	retroceder(){
		console.log("entra");
		this._router.navigate(['/planilla/' + GlobalConfig.planillaId]);
	}


}