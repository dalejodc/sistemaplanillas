import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { CentroDeCostos } from '../models/centro_de_costos'
import { CentroDeCostosService } from '../services/centro_de_costos.service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { GlobalConfig } from  '../services/global-config'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'centroDeCostos',
	templateUrl: '../views/centroDeCostos-list.component.html',
	providers: [CentroDeCostosService]
})

export class CentroDeCostosComponent{
	public listCentros: CentroDeCostos[];

	constructor(
		private _centroCService: CentroDeCostosService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	


		){

	}

	ngOnInit(){
		this.getCentrosCostos();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getCentrosCostos(){
		this._centroCService.getCentroCostos().subscribe(
			response =>{
				this.listCentros = response;
				console.log(this.listCentros);
				

			},
			error =>{
				console.log(error);
			}
			);
	}




}
