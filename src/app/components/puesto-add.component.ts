import { Component } from '@angular/core'
import { Puesto } from '../models/puesto'
import { NivelEstructura } from '../models/nivel_estructura'
 
import { PuestoService} from '../services/puesto.service'
import { NivelEstructuraService } from '../services/nivel-estructura.service'

import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'puesto-add',
	templateUrl : '../views/puesto-add.component.html',
	providers: [NivelEstructuraService,PuestoService] 
})

export class PuestoAdd{
	public p : Puesto; 
	public asignacionComision :Puesto[]
	public nivelEstructura: NivelEstructura;
	public listnivelEstructura: NivelEstructura[];

	constructor(
		private _puestoService: PuestoService,
	  	private _nivelEstructuraService: NivelEstructuraService	,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

	  this.nivelEstructura = new NivelEstructura(0,0,null,null,null,'',null);
	  this.p = new Puesto(null,this.nivelEstructura.nivel_estructura_id,null,'','', true,false); //cuando este el nivel_estructura
			
		this.asignacionComision = [];

	}

	ngOnInit(){
		this.getNivelesEstructura();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
	}

	
	getNivelesEstructura(){
		this._nivelEstructuraService.getNivelesEstructura().subscribe(
			response =>{
				this.listnivelEstructura = response;
				console.log(this.listnivelEstructura);

			},
			error=>{
				console.log(<any>error);
			}

		);
	}
	
	//Método para verificar la selección o deselección del box
	onCheckboxChange(id, event) {
		if(event.target.checked) {
		this.p.puesto_tiene_comision=true;
		  
		} else {
		
		  	 this.p.puesto_tiene_comision=false;
		  }
		}

	onSubmit(){
		this._puestoService.addPuesto(this.p).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡El puesto ' + this.p.puesto_nombre+ ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/puesto']);
				},

				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error =(!'
	      		});
				}

			);

	}

}