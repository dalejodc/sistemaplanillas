import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Subject } from 'rxjs/Rx';

declare var $:any;
declare var Materialize:any;

import { TipoCorreoService } from '../services/tipo-correo.service';
import { TipoCorreo } from '../models/tipo_correo';

@Component({
	selector: 'tipo-correo',
	templateUrl: '../views/tipo-correo-list.component.html',
	providers: [TipoCorreoService]
})

export class TipoCorreoComponent {

	public listaCorreos : TipoCorreo[];
	public correo: TipoCorreo;

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _tipoCorreoService: TipoCorreoService,
		private alertService: SweetAlertService	
	){

		this.dtOptions = {
		    pageLength: 10,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getTiposCorreos();
	}

	getTiposCorreos(){
		this._tipoCorreoService.getTiposCorreos().subscribe(
			result => {
				this.listaCorreos = result;
				console.log(this.listaCorreos);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}