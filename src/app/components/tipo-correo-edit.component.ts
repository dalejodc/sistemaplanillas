import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { TipoCorreoService } from '../services/tipo-correo.service';
import { TipoCorreo } from '../models/tipo_correo';

@Component({
	selector: 'tipo-correo-edit',
	templateUrl: '../views/tipo-correo-add.component.html',
	providers: [TipoCorreoService]
})

export class TipoCorreoEditComponent{
	public tipoCorreo : TipoCorreo;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tipoCorreoService: TipoCorreoService,
		private alertService: SweetAlertService	
	){
		this.tipoCorreo = new TipoCorreo(null,null, null);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();
		this.getCorreo();
	}

	getCorreo(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._tipoCorreoService.getCorreo(id).subscribe(
				response=>{
						this.tipoCorreo= response;
						console.log(this.tipoCorreo);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarCorreo(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._tipoCorreoService.editCorreo(id, this.tipoCorreo).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El tipo de correo ' + this.tipoCorreo.tipo_correo_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/tipo-correo']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}