import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { RentaService } from '../services/renta.service';
import { PeriodoService } from '../services/periodo.service';
import { Renta } from '../models/renta';
import { Periodo } from '../models/periodo';

@Component({
	selector: 'renta-add',
	templateUrl: '../views/renta-add.component.html',
	providers: [RentaService, PeriodoService]
})

export class RentaAddComponent {

	public renta: Renta;
	public listaPeriodos: Periodo[];

	constructor(
		private _rentaService: RentaService,
		private _periodoService: PeriodoService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.renta= new Renta ( null, null, null, null, null, null, null);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getPeriodos();
	}

	guardarRenta(){
		console.log(this.renta);

		this._rentaService.addRenta(this.renta).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡La renta ' + this.renta.renta_nombre+ ' fue guardada exitosamente!'
	      			});
				this._router.navigate(['/renta']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}

	getPeriodos(){
		this._periodoService.getPeriodos().subscribe(
			result =>{
				this.listaPeriodos = result;
				console.log(this.listaPeriodos);
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}