import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { EmpresaService } from '../services/empresa.service';
import { Empresa } from '../models/empresa';
import { NivelEstructuraService } from '../services/nivel-estructura.service';
import { PlanService } from '../services/plan.service';

@Component({
	selector: 'empresa-edit',
	templateUrl: '../views/empresa-add.component.html',
	providers: [EmpresaService, NivelEstructuraService, PlanService]
})

export class EmpresaEditComponent{
	public empresa : Empresa;
	public paises: number;
	public planes: number;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _empresaService: EmpresaService,
		private _paisService: NivelEstructuraService,
		private _planService: PlanService,
		private alertService: SweetAlertService	
	){
	 	this.empresa= new Empresa(null, 1, null, null, null, null,null,null, null, null);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getEmpresa();
		this.getPaises();
		this.getPlanes();
	}

	getEmpresa(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._empresaService.getEmpresa(id).subscribe(
				response=>{
						this.empresa= response;
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	getPaises(){
		this._paisService.getPaises().subscribe(
			result => {
				this.paises = result;
				$('select').material_select();
				// console.log(this.paises);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getPlanes(){
		this._planService.getPlanes().subscribe(
			result => {
				this.planes = result;
				// console.log(this.planes);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	guardarEmpresa(){
		this.empresa.estado_id =1;
	
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._empresaService.editEmpresa(id, this.empresa).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡La empresa ' + this.empresa.empresa_nombre+ ' fue actualizada exitosamente!'
	      			});
					this._router.navigate(['/empresa']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}