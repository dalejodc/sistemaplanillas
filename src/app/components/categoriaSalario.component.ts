import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { CategoriaSalario } from '../models/categoria_salario'
import { CategoriaSalarioService } from '../services/categoria_salario.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'categoriaSalario',
	templateUrl: '../views/categoriaSalario-list.component.html',
	providers: [CategoriaSalarioService]
})

export class CategoriaSalarioComponent{
	public listcategoriaSalarios : CategoriaSalario[];

	constructor(
		private _categoriaSalarioService: CategoriaSalarioService,
		private alertService: SweetAlertService	

		){

	}

	ngOnInit(){
		this.getCategoriaSalarios();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getCategoriaSalarios(){
		this._categoriaSalarioService.getCategoriaSalarios().subscribe(
			response =>{
				this.listcategoriaSalarios = response;
				console.log(this.listcategoriaSalarios);
			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	onDelete(id, categoria: CategoriaSalario){
		
  
		this.alertService.confirm({
	      title: '¿Desea Deshabilitar esta categoria de Salario: '+categoria.categoria_salario_nombre+'?',
	      text: "La categoria se deshabilitará y no podrá asignarse a los puestos.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	   /* .then(()=> {
	    	this._categoriaSalarioService.deleteCategoriaSalario(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡La categoria' + categoria.categoria_salario_nombre + ' fue deshabilitada exitosamente!'
					});
					this.getCategoriaSalarios();
				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text: categoria.categoria_salario_nombre + error.statusText
	      			});
					console.log(<any>error)
					this.getCategoriaSalarios();
				}
			);
	    })*/

 .then(()=> {
	    	
	    	categoria.categoria_salario_estado = false;

	    	this._categoriaSalarioService.deleteCategoriaSalario(id).subscribe(
				response =>{
					
					this.alertService.success({
					title: '¡La categoria de Salario' + categoria.categoria_salario_nombre + ' fue deshabilitada exitosamente!'
					});

					this.getCategoriaSalarios();
				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error)
				}

			);

	    })

	    .catch(() => console.log('canceled'));		
	}
}