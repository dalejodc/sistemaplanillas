import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { EstadoCivilService } from '../services/estado-civil.service';
import { EstadoCivil } from '../models/estado_civil';

@Component({
  selector: 'estado-civil-add',
  templateUrl: '../views/estado-civil-add.component.html',
  providers: [EstadoCivilService]
})
export class EstadoCivilAddComponent implements OnInit {

  public estadoCivil: EstadoCivil;

	constructor(
		private _estadoCivilService: EstadoCivilService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.estadoCivil= new EstadoCivil ( null, null, null, true);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
	}

	guardarEstadoCivil(){
		console.log(this.estadoCivil);

		this._estadoCivilService.addEstadoCivil(this.estadoCivil).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El estado civil ' + this.estadoCivil.estado_civil_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/estado-civil']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}

}
