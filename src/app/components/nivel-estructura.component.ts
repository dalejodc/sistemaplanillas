import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

declare var $:any;
declare var Materialize:any;

import { NivelEstructuraService } from '../services/nivel-estructura.service';
import { NivelEstructura } from '../models/nivel_estructura';

@Component({
	selector: 'nivel-estructura',
	templateUrl: '../views/nivel-estructura.component.html',
	providers: [NivelEstructuraService]
})

export class NivelEstructuraComponent {

	public listaNiveles : NivelEstructura[];
	public nivelEstructura: NivelEstructura;
	public mensaje: string;
	public nombre_nivel: string;
	public padre: number;
	public nivel: number;
	public mostrar: boolean;
	public mostrar_para_depto: boolean;

	constructor(
		private _nivelService: NivelEstructuraService,
		private alertService: SweetAlertService
	){ 
		this.mostrar = false;
		this.mostrar_para_depto = false;
		this.mensaje ="";
		this.nombre_nivel ="";
	}
	
	ngOnInit(){
		$('.collapsible-header').collapsible();
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		this.getNivelesEstructura();
	}

	getNivelesEstructura(){
		this._nivelService.getNivelesEstructura().subscribe(
			result => {
				this.listaNiveles = result;
				console.log(this.listaNiveles);
				this.mostrar = false;
			},
			error => {
				console.log(<any>error);
			}
			);
	}
	mostrar2(){
		this.mostrar_para_depto = true;
	}
	agregar(x){
		this.mostrar = true;
		this.mensaje = "Agregará un sub elemento en " +x.nivel_estructura_nombre;
		this.padre = x.nivel_estructura_id;
		this.nivel = x.tipo_nivel_id +1;
	}

	agregarDepartamento(){
		interface NIVEL {
			    niv_nivel_estructura_id: number,
			    tipo_nivel_id : number,
			    nivel_estructura_nombre: string,
			};

		//Creando objeto auxiliar para que sea aceptado en el formato de la URL
		const niv : NIVEL = {
				niv_nivel_estructura_id: null,
			    tipo_nivel_id : 1,
			    nivel_estructura_nombre: this.nombre_nivel,
		};

		this._nivelService.addEstructura(niv).subscribe(
			result=>{
				this.mostrar_para_depto = false;
				this.ngOnInit();
				this.nombre_nivel ="";
			}, 
			error=>{
				console.log(<any>error);
			});
	}
	
	cancelar(){
		this.mostrar = false;
		this.mostrar_para_depto = false;
		this.nombre_nivel = "";
	}

	guardarNivel(){
		// console.log(this.nombre_nivel);

		interface NIVEL {
			    niv_nivel_estructura_id: number,
			    tipo_nivel_id : number,
			    nivel_estructura_nombre: string,
			};

		//Creando objeto auxiliar para que sea aceptado en el formato de la URL
		const niv : NIVEL = {
				niv_nivel_estructura_id: this.padre,
			    tipo_nivel_id : this.nivel,
			    nivel_estructura_nombre: this.nombre_nivel,
		};

		console.log(JSON.stringify(niv));

		this._nivelService.addEstructura(niv).subscribe(
			result=>{
				this.mostrar = false;
				this.nombre_nivel ="";
				this.ngOnInit();
			}, 
			error=>{
				console.log(<any>error);
			});
	}
}
