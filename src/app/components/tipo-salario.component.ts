import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { TipoSalario } from '../models/tipo_salario';
import { TipoSalarioService } from '../services/tipo-salario.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'tipoSalario',
	templateUrl: '../views/tipoSalario-list.component.html',
	providers: [TipoSalarioService]
})

export class TipoSalarioComponent{
	public listipoSalarios : TipoSalario[];

	constructor(
		private _tipoSalarioService: TipoSalarioService,
		private alertService: SweetAlertService	

		){

	}

	ngOnInit(){
		this.getTipoSalarios();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getTipoSalarios(){
		this._tipoSalarioService.getTipoSalarios().subscribe(
			response =>{
				this.listipoSalarios = response;
				console.log(this.listipoSalarios);
			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	onDelete(id, tipo: TipoSalario){
		
  
		this.alertService.confirm({
	      title: '¿Desea Eliminar el tipo de Salario: '+tipo.tipo_salario_nombre+'?',
	      text: "El tipo de Salario se eliminara.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
	    	this._tipoSalarioService.deleteTipoSalario(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡El tipo' + tipo.tipo_salario_nombre + ' fue eliminado exitosamente!'
					});
					this.getTipoSalarios();
				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text: tipo.tipo_salario_nombre + error.statusText
	      			});
					console.log(<any>error)
					this.getTipoSalarios();
				}
			);
	    })

	    .catch(() => console.log('canceled'));		
	}
}