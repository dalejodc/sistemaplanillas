import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Subject } from 'rxjs/Rx';

declare var $:any;
declare var Materialize:any;

import { TipoTelefonoService } from '../services/tipo-telefono.service';
import { TipoTelefono } from '../models/tipo_telefono';

@Component({
	selector: 'tipo-telefono',
	templateUrl: '../views/tipo-telefono-list.component.html',
	providers: [TipoTelefonoService]
})

export class TipoTelefonoComponent {

	public listaTelefonos : TipoTelefono[];
	public telefono: TipoTelefono;

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _tipoTelefonoService: TipoTelefonoService,
		private alertService: SweetAlertService	
	){

		this.dtOptions = {
		    pageLength: 10,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getTiposTelefonos();
	}

	getTiposTelefonos(){
		this._tipoTelefonoService.getTiposTelefonos().subscribe(
			result => {
				this.listaTelefonos = result;
				console.log(this.listaTelefonos);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}