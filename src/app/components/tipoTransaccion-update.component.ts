import { Component } from '@angular/core'
import { TipoTransaccion } from '../models/tipo_transaccion'
import { TipoTransaccionService} from '../services/tipo_transaccion.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'tipoTransaccion-update',
	templateUrl : '../views/tipoTransaccion-update.component.html',
	providers : [TipoTransaccionService]
})

export class TipoTransaccionUpdate{
	public t : TipoTransaccion;

	constructor(
		private _tipoTransaccionService: TipoTransaccionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.t = new TipoTransaccion(0,'','',false,'');


	}

	ngOnInit(){
		this.getTipoTransaccion();
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();

	}

	getTipoTransaccion(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._tipoTransaccionService.getTipoTransaccion(id).subscribe(
				response =>{
					this.t = response;		
				},
				error => {
				})

		});
	}

	onSubmit(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

		this._tipoTransaccionService.updateTipoTransaccion(id, this.t).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El tipo de transacción ' + this.t.tipo_transaccion_nombre+ ' fue editada exitosamente!'
	      			});
					this._router.navigate(['/tipotransaccion']);
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'});

				}
			);
	
		});

	}
}