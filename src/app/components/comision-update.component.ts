import { Component } from '@angular/core'
import { Comision } from '../models/comision'
import { ComisionService} from '../services/comision.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'comision-update',
	templateUrl : '../views/comision-update.component.html',
	providers : [ComisionService]
})

export class ComisionUpdate{
	public p : Comision;
	

	constructor(
		private _comisionService: ComisionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.p = new Comision(0,null,null, null, null);

	}

	ngOnInit(){
		this.getComision();

		$('.collapsible').collapsible();
 	       $(".button-collapse").sideNav();

	}

	getComision(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._comisionService.getComision(id).subscribe(
				response =>{
					this.p = response;
					this.p.comision_porcentaje = this.p.comision_porcentaje * 100;
					
					console.log(this.p);		
				},
				error => {
				})

		});

	}

	onSubmit(){

		if (this.p.comision_minimo<this.p.comision_maximo){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
		this.p.comision_porcentaje = this.p.comision_porcentaje / 100;
		this._comisionService.updateComision(id, this.p).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡La comisión con porcentaje ' + this.p.comision_nombre+ ' fue editada exitosamente!'
	      			});
					this._router.navigate(['/comision']);
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'});

				}
			);
	
		});

	}

	else{

		this.alertService.error({
	        			title: '¡El mínimo y el máximo no son válidos !'});

	}
	}
}