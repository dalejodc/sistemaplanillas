import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'


import { Puesto } from '../models/puesto'
//import { NivelEstructura } from '../models/nivel_estructura'

import { PuestoService} from '../services/puesto.service'
//import { NivelEstructuraService } from '../services/nivel_estructura.service'

declare var $:any;
declare var Materialize:any;
@Component({
	selector : 'puesto',
	templateUrl: '../views/puesto-list.component.html',
	providers: [PuestoService]
})

export class PuestoComponent{
	public listaPuestos : Puesto[];


	constructor (
		private _puestoService: PuestoService,
		private alertService: SweetAlertService	
		){
	
	}

	ngOnInit(){
		this.getPuestos()
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
	}

	getPuestos(){
		this._puestoService.getPuestos().subscribe(
			response =>{
				this.listaPuestos = response;
				console.log(this.listaPuestos)
			},
			error =>{
				console.log(<any>error)
			}
		);
	}


	onDelete(id, puesto: Puesto){
		

		this.alertService.confirm({
	      title: '¿Desea deshabilitar puesto: '+puesto.puesto_nombre+'?',
	      text: "El puesto se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })


	    .then(()=> {
	    	
	    	puesto.puesto_estado = false;

	    	this._puestoService.deletePuesto(id, puesto).subscribe(
				response =>{
					
					this.alertService.success({
					title: '¡El puesto ' + puesto.puesto_nombre + ' fue deshabilitado exitosamente!'
					});

					this.getPuestos();
				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error)
				}

			);

	    })
	    .catch(() => console.log('canceled'));	

	}
	

}
