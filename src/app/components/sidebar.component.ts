import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
// import $ from 'jquery';
declare var jQuery:any;

import { LoginService } from '../services/login.service';
import { Menu } from '../models/menu';
import { Usuario } from '../models/usuario';
import { GlobalConfig } from  '../services/global-config'

@Component({
    selector: 'sidebar',
    templateUrl: '../views/sidebar.component.html',
    providers: [LoginService]
})

export class SidebarComponent implements OnInit{

    public url = GlobalConfig.url;
    public usuario : Usuario
    public opcionesMenu : Menu[];
    public menu_config : boolean;

    constructor(
        private _router: Router,
        private _loginService: LoginService,
        private _http: Http
    ){
       this.usuario = new Usuario (null, null);
       this.menu_config = true;
    }
    
    ngOnInit(){
      if(sessionStorage.getItem('rol_id') != '1'){
         this.menu_config = false;
       }

       this.usuario.usuario_nombre = sessionStorage.getItem('user');
       this.usuario.usuario_contrasenia = sessionStorage.getItem('pass');
       this.getMenu();       
    }

    logOut(){
        sessionStorage.removeItem('token');
        console.log("Eliminado?",localStorage.getItem('token'));
        this._router.navigate(['/']);
     }

    getMenu(){
    let jsonUser = JSON.stringify(this.usuario); //Convirtiendo el objeto a JSON
    let headers = new Headers({
      'Content-type': 'application/json',
      'Authotization': sessionStorage.getItem('token')
    });

    return this._http.post(this.url + 'login', jsonUser, { headers: headers })
      .map(res => res.json())
      .subscribe(
          data => {
            // console.log(data);
            this.opcionesMenu = data.menus;
            // console.log(this.opcionesMenu);
          },
          error => {
            console.log(error);
            console.log(error._body);
          }
        );
    }
}