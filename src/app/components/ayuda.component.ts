import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'ayuda',
	templateUrl: '../views/ayuda.component.html'
})

export class AyudaComponent {

	constructor(
		private alertService: SweetAlertService
	){ }
	
	ngOnInit(){
		$(".button-collapse").sideNav({menuWidth: 250});
		$('.collapsible-header').collapsible();
		$('.collapsible').collapsible();
	}
}