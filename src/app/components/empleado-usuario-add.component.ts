import { Component } from '@angular/core'
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service'
import { Subject } from 'rxjs/Rx';

import { Empleado } from '../models/empleado'
import { EmpladoService } from '../services/empleado.service'
import { UsuarioService } from '../services/usuario.service'
import { RolService } from '../services/rol.service'
import { Rol } from '../models/rol'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'empleado-usuario-add',
	templateUrl: '../views/empleado-usuario-add.component.html',
	providers: [EmpladoService, RolService, UsuarioService]
})

export class EmpleadoUsuarioAddComponent{

	public listEmpleados: Empleado[];
	public listaRoles : Rol[];
	public listaCorreos : any;
	public empleado_id: number;
	public rol_id: number;
	public correo: string;
	public nombre: string;
	public puesto: string;
	public puesto_descripcion: string;
	public fecha: Date;

	constructor(
		private _empleadoService: EmpladoService,
		private _usuarioService: UsuarioService,
		private _route: ActivatedRoute,
		private _router: Router,
		private _rolService: RolService,
		private alertService: SweetAlertService	
		){

			this.nombre ="";
			this.puesto ="";
			this.puesto_descripcion ="";
		}

	ngOnInit(){
		this.getEmpleado();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
		this.getRoles();
	}

	getRoles(){
		this._rolService.getRoles().subscribe(
			result => {
				this.listaRoles = result;
				console.log(this.listaRoles);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getEmpleado(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._empleadoService.getEmpleado(id).subscribe(
				response =>{
					console.log(response);
					this.nombre = response.persona.persona_primer_nombre + " "+ response.persona.persona_segundo_nombre 
					+" "+ response.persona.persona_primer_apellido +" "+ response.persona.persona_segundo_nombre;

					this.puesto = response.puesto.puesto_nombre;
					this.puesto_descripcion = response.puesto.puesto_descripcion;
					this.fecha = response.empleado_fecha_ingreso;
					this.listaCorreos = response.persona.correos;
					console.log("Correos:", this.listaCorreos);
					this.empleado_id = response.empleado_id;
				},
				error =>{
					console.log(<any>error);
				}
			);
		});	
	}

	guardar(){
		interface USUARIO {
			    empleado_id: number,
			    rol_id: number,
			    correo_direccion: string
			};

		//Creando objeto auxiliar para que sea aceptado en el formato de la URL
		const us : USUARIO = {
			empleado_id: this.empleado_id,
			rol_id: this.rol_id,
			correo_direccion: this.correo
		};

		console.log(JSON.stringify(us));

		this._usuarioService.addUsuario(us).subscribe(
			response=>{
				console.log("exito");
			}, 
			error=>{
				console.log(<any>error);
			}
		);
	}

}
