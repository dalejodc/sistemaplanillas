import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { Movimiento } from '../models/movimiento'
import { MovimientoService } from '../services/movimiento.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'movimiento',
	templateUrl: '../views/movimiento-list.component.html',
	providers: [MovimientoService]
})

export class MovimientoComponent{
	public listaMovimientos : Movimiento[];

	constructor(
		private _movimientoService: MovimientoService,
		private alertService: SweetAlertService	

		){

	}

	ngOnInit(){
		this.getMovimientos();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getMovimientos(){
		this._movimientoService.getMovimientos().subscribe(
			response =>{
				this.listaMovimientos = response;
				console.log(this.listaMovimientos);
			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	onDelete(id, movimiento: Movimiento){
		

		this.alertService.confirm({
	      title: '¿Desea eliminar el movimiento: '+movimiento.movimiento_nombre+'?',
	      text: "El movimiento se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
	    	this._movimientoService.deleteMovimiento(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡El movimiento' + movimiento.movimiento_nombre + ' fue deshabilitado exitosamente!'
					});
					this.getMovimientos();

				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text: movimiento.movimiento_nombre + error.statusText
	      			});
					console.log(<any>error)
					this.getMovimientos();
				}


			);

	    })
	    .catch(() => console.log('canceled'));		
	}
}