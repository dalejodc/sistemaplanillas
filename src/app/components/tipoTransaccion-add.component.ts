import { Component } from '@angular/core'
import { TipoTransaccion } from '../models/tipo_transaccion'
import { TipoTransaccionService} from '../services/tipo_transaccion.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'tipoTransaccion-add',
	templateUrl : '../views/tipoTransaccion-add.component.html',
	providers: [TipoTransaccionService]
})

export class TipoTransaccionAdd{
	public t : TipoTransaccion;

	constructor(
		private _tipoTransaccionService: TipoTransaccionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.t = new TipoTransaccion(0,'','',false,'');

	}

	ngOnInit(){
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();

	}

	onSubmit(){
		console.log(this.t);
		this._tipoTransaccionService.addTipoTransaccion(this.t).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡El tipo de transacción ' + this.t.tipo_transaccion_nombre+ ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/tipotransaccion']);
				},

				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				}

			);

	}

}