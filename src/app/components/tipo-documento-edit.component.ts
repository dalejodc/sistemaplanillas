import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { TipoDocumentoService } from '../services/tipo-documento.service';
import { TipoDocumento } from '../models/tipo_documento';

@Component({
	selector: 'tipo-correo-edit',
	templateUrl: '../views/tipo-documento-add.component.html',
	providers: [TipoDocumentoService]
})

export class TipoDocumentoEditComponent{
	public tipoDocumento : TipoDocumento;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tipoDocumentoService: TipoDocumentoService,
		private alertService: SweetAlertService	
	){
		this.tipoDocumento = new TipoDocumento(null,null, null, null, true);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();
		this.getDocumento();
	}

	getDocumento(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._tipoDocumentoService.getDocumento(id).subscribe(
				response=>{
						this.tipoDocumento= response;
						console.log(this.tipoDocumento);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarDocumento(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._tipoDocumentoService.editCorreo(id, this.tipoDocumento).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El tipo de documento ' + this.tipoDocumento.tipo_documento_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/tipo-documento']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}