import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'
import { Router, ActivatedRoute, Params } from '@angular/router'

import { CategoriaSalario } from '../models/categoria_salario'
import { Puesto } from '../models/puesto'
import { Empresa } from '../models/empresa'

import { CategoriaSalarioService } from '../services/categoria_salario.service'
import { PuestoService } from '../services/puesto.service'

import { EmpresaService } from '../services/empresa.service';

declare var $:any;
declare var Materialize:any;



@Component({
	selector: 'categoriaSalario-add',
	templateUrl: '../views/categoriaSalario-add.component.html',
	providers: [PuestoService,EmpresaService, CategoriaSalarioService]
})

export class CategoriaSalarioAdd{
	public categoriaSalario: CategoriaSalario;
	public puesto: Puesto;
	public listaPuestos: Puesto[];
	
	//public listaEmpresas : Empresa[];
	public empresa: Empresa;

	constructor(
		
		private _categoriaSalarioService: CategoriaSalarioService,
		private _puestoService: PuestoService,
		private _empresaService: EmpresaService,

		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService	

		){
		//this.empresa= new Empresa(null, 3, null, null, null, null,null,null, null, null);
		this.puesto = new Puesto(null,null,null,null,null,null,null);
		this.categoriaSalario = new CategoriaSalario(0, this.puesto.puesto_id,null,
													 '','',null,null,true);
 
	}

	ngOnInit(){
		this.getPuestos();
		this.getEmpresas();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getPuestos(){
		this._puestoService.getPuestos().subscribe(
			response =>{
				this.listaPuestos = response;
				console.log(this.listaPuestos);

			},
			error=>{
				console.log(<any>error);
			}

		);
	}

	getEmpresas(){
		this._empresaService.getEmpresas().subscribe(
			result => {
				this.empresa = result;
				console.log(this.empresa);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	onSubmit(){
		
		if (this.categoriaSalario.categoria_salario_minimo<this.categoriaSalario.categoria_salario_maximo){
		this._categoriaSalarioService.addCategoriaSalario(this.categoriaSalario).subscribe(
			response=>{

				this.alertService.success({
	        			title: '¡La categoria de Salario ' + this.categoriaSalario.categoria_salario_nombre+ ' fue guardada exitosamente!'
	      			});
					this._router.navigate(['/categoriaSalario']);

			},
			error => {
				console.log(<any>error);
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});


			}
			);

	}

else{

	this.alertService.error({title: '¡El mínimo y el máximo no son válidos !'});



}
}
}