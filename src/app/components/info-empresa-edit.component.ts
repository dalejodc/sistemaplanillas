import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { InfoEmpresaService } from '../services/info-empresa.service';
import { InfoEmpresa } from '../models/info_empresa';

@Component({
	selector: 'empresa-edit',
	templateUrl: '../views/info-empresa-edit.component.html',
	providers: [InfoEmpresaService]
})

export class InfoEmpresaEditComponent{
	public empresa : InfoEmpresa;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _empresaService: InfoEmpresaService,
		private alertService: SweetAlertService	
	){
		this.empresa = new InfoEmpresa(null, null, null, null,null,null, null);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getEmpresa();
	}

	getEmpresa(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			console.log(id);

			this._empresaService.getInfoEmpresa().subscribe(
				response=>{
						this.empresa= response;
						console.log(this.empresa);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarEmpresa(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._empresaService.editEmpresa(id, this.empresa).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡La empresa ' + this.empresa.empresa_nombre+ ' fue actualizada exitosamente!'
	      			});
					this._router.navigate(['/mi-empresa']);			
				},
				error =>{
					// this._router.navigate(['/mi-empresa']);	this.alertService.success({
	    //     			title: '¡La empresa ' + this.empresa.empresa_nombre+ ' fue actualizada exitosamente!'
	    //   			});
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}