import { Component } from '@angular/core'
import { Profesion } from '../models/profesion'
import { ProfesionService} from '../services/profesion.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'profesion-update',
	templateUrl : '../views/profesion-update.component.html',
	providers : [ProfesionService]
})

export class ProfesionUpdate{
	public p : Profesion;
	public prueba: string;

	constructor(
		private _profesionService: ProfesionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.p = new Profesion(0,'','', true);


	}

	ngOnInit(){
		this.getProfesion();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getProfesion(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._profesionService.getProfesion(id).subscribe(
				response =>{
					this.p = response;		
				},
				error => {
				})

		});
	}

	onSubmit(){

		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

		this._profesionService.updateProfesion(id, this.p).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡La profesión ' + this.p.profesion_nombre+ ' fue editada exitosamente!'
	      			});
					this._router.navigate(['/profesion']);
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'});

				}
			);
	
		});

	}
}