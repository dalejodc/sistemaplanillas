import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { PeriodoPlanilla } from '../models/periodoPlanilla'
import { Planilla } from '../models/planilla'
import { PlanillaService } from '../services/planilla.service'
import { EmpresaService } from '../services/empresa.service'
import { InfoEmpresaService } from '../services/info-empresa.service'
import { NivelEstructuraService } from '../services/nivel-estructura.service'

import { Empresa } from '../models/empresa'

declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'verplanilla',
	templateUrl: '../views/ver_planilla.component.html',
	providers: [PlanillaService, EmpresaService, InfoEmpresaService, NivelEstructuraService]
})

export class VerplanillaComponent{
	public listPlanillas: Planilla[];
	public listPeriodos: PeriodoPlanilla[];
	public periodoPlanilla: PeriodoPlanilla;
	public listaDepartamentos: any;
	public empresa: Empresa;
	public idPeriodo: number;
	consultando: boolean;
	public department: string;
	public total: number;
	public fecha_inicio: Date = new Date();

	constructor(
		private _planillaService: PlanillaService,
		private alertService: SweetAlertService,
		private _empresaService: EmpresaService,
		private _infoEmpresaService: InfoEmpresaService,
		private _nivelService: NivelEstructuraService


		){
		this.idPeriodo = null;
		this.consultando = false;

		//this.periodoPlanilla=new PeriodoPlanilla(null,null,new Date(),null,null);
	}

	ngOnInit(){
		this.getEmpresa();
		this.getPeriodos();
		this.getNiveles();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();
		//this.periodoPlanilla.planilla[1].detalleMovimiento[1].movimiento.tipoTransaccion.tipo_transaccion_signo

	}


	getEmpresa(){
		this._infoEmpresaService.getEmpresa().subscribe(
			result => {
				this.empresa = result;
				console.log(this.empresa);

			},
			error => {
				console.log(<any>error);
			}
			);
		}


	getNiveles(){
		this._nivelService.getNivelesDepartamentos().subscribe(
			response =>{
				this.listaDepartamentos = response;
				console.log("Niveles", this.listaDepartamentos);
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	getPeriodos(){
		this._planillaService.getPeriodoPlanilla().subscribe(
			response =>{
				this.listPeriodos = response;
				console.log(this.listPeriodos);

			},
			error =>{
				console.log(error);
			}
			);
	}

	guardarPeriodo(){
			this._planillaService.guardarPeriodo(this.fecha_inicio).subscribe(
				response=>{
					this.getPeriodos();
					this.alertService.success({
						title: 'El primer periodo fue creado'
					});
				},
				error=>{
					this.alertService.error({
						title: '¡Ocurrío un error'
					});
					console.log(<any>error);
				}
			);
		}

	getPlaniallaByPeriodoId(){
		if (this.idPeriodo){
			this.consultando = true;

			this._planillaService.getPlanillasByPeriodoId(this.idPeriodo).subscribe(
			response =>{
				this.periodoPlanilla = response;
				console.log(this.periodoPlanilla);
				this.consultando = false;
				this.calcularTotal();


			},
			error =>{
				console.log(error);
			}
			);

		}
		else{
			this.alertService.error({
	        			title: '¡Debe seleccionar un periodo!'});

		}

	}

	calcularTotal(){

		for (let i = 0; i<= this.periodoPlanilla.planilla.length; i++){
			console.log(this.periodoPlanilla.planilla[i].planilla_total);

		}
	}

	closePeriodo(){

		this.alertService.confirm({
	      title: '¿Desea cerrar el periodo? ',
	      text: "El periodo se cerrará",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
				this._planillaService.closePeriodo().subscribe(
					response =>{
						this.alertService.success({
							title: 'Periodo cerrado'
						});
						this.getPeriodos();
					},
					error =>{
						this.alertService.error({
			        			title: '¡Error'});
					}

					);
				 })
	    .catch(() => console.log('canceled'));
	}



}
