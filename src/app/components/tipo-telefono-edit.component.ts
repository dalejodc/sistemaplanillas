import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;


import { TipoTelefonoService } from '../services/tipo-telefono.service';
import { TipoTelefono } from '../models/tipo_telefono';

@Component({
	selector: 'tipo-telefono-edit',
	templateUrl: '../views/tipo-telefono-add.component.html',
	providers: [TipoTelefonoService]
})

export class TipoTelefonoEditComponent{
	public tipoTelefono : TipoTelefono;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _tipoTelefonoService: TipoTelefonoService,
		private alertService: SweetAlertService	
	){
		this.tipoTelefono = new TipoTelefono(null,null, null);
	}

	ngOnInit(){
		$(document).ready(function() { Materialize.updateTextFields(); });
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getTelefono();
	}

	getTelefono(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._tipoTelefonoService.getTelefono(id).subscribe(
				response=>{
						this.tipoTelefono= response;
						console.log(this.tipoTelefono);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	guardarTelefono(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._tipoTelefonoService.editTelefono(id, this.tipoTelefono).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El tipo de telefono ' + this.tipoTelefono.tipo_telefono_nombre+ ' fue actualizado exitosamente!'
	      			});
					this._router.navigate(['/tipo-telefono']);			
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);
		});	
	}
}