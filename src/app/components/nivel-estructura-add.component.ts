import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

declare var $:any;
declare var Materialize:any;

import { NivelEstructuraService } from '../services/nivel-estructura.service';
import { NivelEstructura } from '../models/nivel_estructura';

@Component({
	selector: 'nivel-estructura',
	templateUrl: '../views/nivel-estructura-add.component.html',
	providers: [NivelEstructuraService]
})

export class NivelEstructuraAddComponent {

	public listaNiveles : NivelEstructura[];
	public nivelEstructura: NivelEstructura;

	constructor(
		private _nivelService: NivelEstructuraService,
		private alertService: SweetAlertService
	){ }
	
	ngOnInit(){
		$('.collapsible-header').collapsible();
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		this.getNivelesEstructura();
	}

	getNivelesEstructura(){
		this._nivelService.getNivelesEstructura().subscribe(
			result => {
				this.listaNiveles = result;
				console.log(this.listaNiveles);
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}