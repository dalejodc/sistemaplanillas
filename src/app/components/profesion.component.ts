import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

declare var $:any;
declare var Materialize:any;



import { Profesion } from '../models/profesion'
import { ProfesionService} from '../services/profesion.service'

@Component({
	selector : 'profesion',
	templateUrl: '../views/profesion-list.component.html',
	providers: [ProfesionService]
})

export class ProfesionComponent{
	public listaProfesiones : Profesion[];


	constructor (
		private _profesionService: ProfesionService,
		private alertService: SweetAlertService	
		){
	
	}

	ngOnInit(){
		this.getProfesiones();
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();
	}

	getProfesiones(){
		this._profesionService.getProfesiones().subscribe(
			result =>{
				this.listaProfesiones = result;
			},
			error =>{
				console.log(<any>error);
			}
		);
	}


	onDelete(id, profesion: Profesion){
		

		this.alertService.confirm({
	      title: '¿Desea deshabilitar la Profesión: '+profesion.profesion_nombre+'?',
	      text: "La Profesión se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
	    	profesion.profesion_estado = false;

	    	this._profesionService.deleteProfesion(id, profesion).subscribe(
				response =>{
					
					this.alertService.success({
					title: '¡La Profesión ' + profesion.profesion_nombre + ' fue deshabilitado exitosamente!'
					});

					this.getProfesiones();
				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error)
				}

			);

	    })
	    .catch(() => console.log('canceled'));	

	}
	

}