import { Component } from '@angular/core'
import { Profesion } from '../models/profesion'
import { ProfesionService} from '../services/profesion.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'profesion-add',
	templateUrl : '../views/profesion-add.component.html',
	providers: [ProfesionService]
})

export class ProfesionAdd{
	public p : Profesion;

	constructor(
		private _profesionService: ProfesionService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.p = new Profesion(0,'','', true);

	}

	ngOnInit(){
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	onSubmit(){
		this._profesionService.addProfesion(this.p).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡La profesión ' + this.p.profesion_nombre+ ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/profesion']);
				},

				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				}

			);

	}

}