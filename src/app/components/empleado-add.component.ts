import { Component } from '@angular/core'

import { Empleado } from '../models/empleado'
import { Persona } from '../models/persona'
import { Genero } from '../models/genero'
import { EstadoCivil } from '../models/estado_civil'
import { Profesion } from '../models/profesion'
import { TipoTelefono } from '../models/tipo_telefono'
import { Telefono } from '../models/telefono'
import { TipoCorreo } from '../models/tipo_correo'
import { Correo } from '../models/correo'
import { TipoDocumento } from '../models/tipo_documento'
import { Documento } from '../models/documento'
import { Puesto } from '../models/puesto'
import { DetalleContrato } from '../models/detalleContrato'
import { Salario } from '../models/salario'
import { TipoContrato } from '../models/tipo_contrato'
import { TipoSalario } from '../models/tipo_salario'
import { Direccion } from '../models/direccion'





import { EmpladoService} from '../services/empleado.service'
import { GeneroService} from '../services/genero.service'
import { EstadoCivilService} from '../services/estado-civil.service'
import { ProfesionService} from '../services/profesion.service'
import { TipoTelefonoService} from '../services/tipo-telefono.service'
import { TipoCorreoService} from '../services/tipo-correo.service'
import { TipoDocumentoService} from '../services/tipo-documento.service'
import { ContratoService} from '../services/contrato.service'
import { PuestoService} from '../services/puesto.service'
import { TipoSalarioService} from '../services/tipo-salario.service'
import { EstructuraTerritorialService } from '../services/estructura-territorial.service'
import { NivelEstructuraService } from '../services/nivel-estructura.service'




import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'empleado-add',
	templateUrl : '../views/empleado-add.component.html',
	providers: [
		EmpladoService,
		GeneroService,
		EstadoCivilService,
		ProfesionService,
		TipoTelefonoService,
		TipoCorreoService,
		TipoDocumentoService,
		ContratoService,
		PuestoService,
		TipoSalarioService,
		EstructuraTerritorialService,
		NivelEstructuraService
		]
})

export class EmpleadoAdd{
	public empleado : Empleado;
	public persona: Persona;
	public generos: Genero[];
	public estadosC: EstadoCivil[];
	public profesiones: Profesion[];
	public puestos: Puesto[];
	public puestos_id: number;
	public contratos: TipoContrato[];
	public tipoSalarios: TipoSalario[];

	public tiposTelefonos: TipoTelefono[];
	public asignacionTelefonos : Telefono[];
	public telefono : Telefono;

	public tiposCorreos: TipoCorreo[];
	public asignacionCorreos : Correo[];
	public correo : Correo;

	public tiposDocumentos: TipoDocumento[];
	public asignacionDocumentos : Documento[];
	public documento : Documento;

	public detalleContrato: DetalleContrato;
	public salario:Salario;
	public direccionObj: Direccion;

	public listaPuestosOrg : any;
	public listaDepartamentosOrg : any;
	public listaPaises : any;
	public listaDepartamentos : any;
	public listaMunicipios : any;
	public listaNiveles : any;
	public pais: number; //auxiliar para traer sub terriorio
	public departamento: number; //auxiliar para traer sub terriorio
	public municipio: number; //auxiliar para traer sub terriorio
	public estructura_territorial_id: number; //para asignar a persona
	public direccion: string; //para asignar a persona
	public nivel: number; // refiere al nivel de la estructura solo aux para seleccionar
	public puesto_id: number; // Asignar este id al puesto en la persona || empleado
	public depto_nombre: string; // Auxiliar para mostrar label
	public nivelUno: string;
	public nivelDos: string;

	public mostrar : boolean;
	public error : boolean;


	public now: Date = new Date();
	public nowM: Date = new Date();

	constructor(

		private _empleadoService: EmpladoService,
		private _generoService: GeneroService,
		private _puestoService: PuestoService,
		private _tipoSalarioService: TipoSalarioService,
		private _contratoService: ContratoService,
		private _estadoService: EstadoCivilService,
		private _profesionService: ProfesionService,
		private _telefonoService: TipoTelefonoService,
		private _correoService: TipoCorreoService,
		private _documentoService: TipoDocumentoService,
		private _estructuraService: EstructuraTerritorialService,
		private _nivelService: NivelEstructuraService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.mostrar = false;
		this.error = false;
		this.nivelUno ="Nivel 1";
		this.nivelDos ="Nivel 2";

		setInterval(() => {this.now = new Date();}, 1);

        this.nowM.setMonth(this.now.getMonth() - 216);

        this.direccionObj = new Direccion(null, null, null, null)


        this.detalleContrato = new DetalleContrato(null,null,null,null,null, null);

        this.salario = new Salario(null, null, null, null,null);

		this.persona = new Persona(null, null, this.direccionObj, null, null, null, null, null, null, '', '', '','', null, null, null, null);

		this.empleado = new Empleado(null, null, this.persona,null, null, this.detalleContrato, null, null,
			null, null,this.salario,null, null, null, null);

		this.telefono = new Telefono(null,null);
		this.asignacionTelefonos = [];

		this.correo = new Correo(null,null);
		this.asignacionCorreos = [];

		this.documento = new Documento(null,null);
		this.asignacionDocumentos = [];


	}

	ngOnInit(){
		this.getGeneros();
		this.getEstadosC();
		this.getProfesiones();
		this.getTiposTelefonos();
		this.getTiposCorreos();
		this.getTiposDocumentos();
		this.getContratos();
		this.getPuestos();
		this.getTipoSalarios();
		this.getPaises();
		this.vailidar();
		this.getNiveles();


		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();
		console.log(this.now);
  		// $('#modal1').opemModal('close');
	}

	vailidar(){
		if( parseInt(sessionStorage.getItem('cant_empleados')) > parseInt(sessionStorage.getItem('max_empleados'))){

			console.log(sessionStorage.getItem('cant_empleados') + "es mayor que"+ sessionStorage.getItem('max_empleados'));
			this.error = true;
		}else{
			this.error = false;
		}

		console.log(sessionStorage.getItem('cant_empleados'));
		console.log(sessionStorage.getItem('max_empleados'));
	}

	getNiveles(){
		this._nivelService.getNivelesDepartamentos().subscribe(
			response =>{
				this.listaDepartamentosOrg = response;
				console.log("Niveles", this.listaDepartamentosOrg);
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	getPuestosbyDepto(id){
		console.log(id);
		this._nivelService.getNivelesPuestosByDepartamento(id).subscribe(
			response =>{
				this.listaPuestosOrg = response;
				console.log("Puestos", this.listaPuestosOrg);
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	//Para la estructura terririal
	getPaises(){
		this._estructuraService.getPaises().subscribe(
			response =>{
				this.listaPaises = response;
				// console.log("Paises", this.listaPaises);
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	getDepartamentos(x){
		this.mostrar = false;
		this._estructuraService.getNiveles(x).subscribe(
			response =>{
				this.listaNiveles = response;
				console.log("niveles", this.listaNiveles);
				this.nivelUno = this.listaNiveles[0].tipo_estructura_nombre;
				this.nivelDos = this.listaNiveles[1].tipo_estructura_nombre;
			},
			error =>{

			}
		);
		this._estructuraService.getDepartamentos(x).subscribe(
			response =>{
				this.listaDepartamentos = response;
				// console.log("Departamentos", this.listaDepartamentos);
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	getMunicipios(x){
		this._estructuraService.getDepartamentos(x).subscribe(
			response =>{
				this.listaMunicipios = response;
				// console.log("Municipios", this.listaMunicipios);
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	setMunicipio(x){
		this.estructura_territorial_id = x; //Asignar esto al objeto direccion
		console.log(this.estructura_territorial_id);

		if(this.municipio != null){
			this.mostrar = true;
		}
	}

	// Para gestionar la lista asignacion de teléfonos
	agregarTelefono(){

		if(this.telefono.telefono_numero == null){
			Materialize.toast('Escriba el teléfono', 2000);
		}else{
			this.asignacionTelefonos.push({"tipo_telefono_id": this.telefono.tipo_telefono_id,"telefono_numero": this.telefono.telefono_numero});
			console.log(this.asignacionTelefonos);
			this.telefono.telefono_numero = null;
			this.telefono.tipo_telefono_id = null;
		}
	}

	quitarTelefono(i){
		console.log("Antes", this.asignacionTelefonos);
		this.asignacionTelefonos.splice(i, 1);
		console.log("Despues", this.asignacionTelefonos);
	}

	getTiposTelefonos(){
		this._telefonoService.getTiposTelefonos().subscribe(
			response =>{
				this.tiposTelefonos = response;
				console.log(this.tiposTelefonos);
			},
			error =>{
				console.log(error)

			}
		);
	}

	ayudaTelefono(){
		this.alertService.info({
			title: '¿Cómo guardar un teléfono',
			text: 'Seleccione el tipo de teléfono, luego digite el número y presione el botón verde.',
		});
	}

	// Para gestionar la lista asignacion de Documentos
	agregarDocumento(){

		if(this.documento.documento_valor == null){
			Materialize.toast('Escriba el número de documento', 2000);
		}else{
			this.asignacionDocumentos.push({"tipo_documento_id": this.documento.tipo_documento_id,"documento_valor": this.documento.documento_valor});
			console.log(this.asignacionDocumentos);
			this.documento.documento_valor = null;
			this.documento.tipo_documento_id = null;
		}
	}

	quitarDocumento(i){
		this.asignacionDocumentos.splice(i, 1);
	}

	getTiposDocumentos(){
		this._documentoService.getTiposDocumentos().subscribe(
			response =>{
				this.tiposDocumentos = response;
				console.log(this.tiposDocumentos);
			},
			error =>{
				console.log(error)

			}
		);
	}

	ayudaDocumento(){
		this.alertService.info({
			title: '¿Cómo guardar un documento?',
			text: 'Seleccione el tipo de documento, luego digite el número y presione el botón verde.',
		});
	}

	// Para gestionar la lista asignacion de correos
	agregarCorreo(){
		if(this.correo.correo_direccion == null){
			Materialize.toast('Escriba la dirección del correo', 2000);
		}else{
			this.asignacionCorreos.push({"tipo_correo_id": this.correo.tipo_correo_id,"correo_direccion": this.correo.correo_direccion});
			console.log(this.asignacionCorreos);
			this.correo.tipo_correo_id = null;
			this.correo.correo_direccion = null;
		}
	}

	quitarCorreo(i){
		this.asignacionCorreos.splice(i, 1);
		console.log("Despues", this.asignacionCorreos);
	}

	getTiposCorreos(){
		this._correoService.getTiposCorreos().subscribe(
			response =>{
				this.tiposCorreos = response;
				console.log(this.tiposCorreos);
			},
			error =>{
				console.log(error)

			}
		);
	}

	ayudaCorreo(){
		this.alertService.info({
			title: '¿Cómo guardar un correo',
			text: 'Seleccione el tipo de correo, luego digite el número y presione el botón verde.',
		});
	}

	//Trayendo los demás selects

	getGeneros(){
		this._generoService.getGeneros().subscribe(
			response =>{
				this.generos = response;
				console.log(this.generos);
			},
			error =>{
				console.log(error)
			}
		);
	}

	getEstadosC(){
		this._estadoService.getEstadoCiviles().subscribe(
			response =>{
				this.estadosC = response;
				console.log(this.estadosC);
			},
			error =>{
				console.log(error)
			}
			);
	}

	getProfesiones(){
		this._profesionService.getProfesiones().subscribe(
			response =>{
				this.profesiones = response;
				console.log(this.profesiones);
			},
			error =>{
				console.log(error)
			}
			);
	}

	getContratos(){
		this._contratoService.getContratos().subscribe(
			response =>{
				this.contratos = response;
				console.log(this.contratos);
			},
			error =>{
				console.log(error)
			}
			);

	}

	getPuestos(){
		this._puestoService.getPuestos().subscribe(
			response =>{
				this.puestos = response;
				console.log(this.puestos);
			},
			error =>{
				console.log(error)
			}
			);
	}

	getTipoSalarios(){
		this._tipoSalarioService.getTipoSalarios().subscribe(
			response =>{
				this.tipoSalarios = response;
				console.log(this.tipoSalarios);
			},
			error =>{
				console.log(error)
			}
			);
	}



	onSubmit(){


		this.empleado.persona.correos = this.asignacionCorreos;
		this.empleado.persona.documentos = this.asignacionDocumentos;
		this.empleado.persona.telefonos = this.asignacionTelefonos;
		this.direccionObj.direccion_nombre = this.direccion;
		this.direccionObj.estructura_territorial_id = this.estructura_territorial_id;
		this.empleado.empleado_fecha_ingreso = this.empleado.detalle_contrato.detalle_contrato_fecha_inicio;
		this.empleado.puesto_id = this.puesto_id;

		console.log(this.empleado);

		console.log(JSON.stringify(this.empleado));


		this._empleadoService.addEmpleado(this.empleado).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡El empleado ' + this.empleado.persona.persona_primer_nombre + ' fue guardado exitosamente!'
	      			});
					this._router.navigate(['/empleado']);
				},

				error =>{
					console.log(error);
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text:  error
	      		});


				}

			);

	}

}
