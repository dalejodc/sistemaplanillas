import { Component } from '@angular/core'
import { DetalleMovimiento } from '../models/detalleMovimiento'
import { Movimiento } from '../models/movimiento'
import { DetalleMovimientoService} from '../services/detalleMovimiento.service'
import { MovimientoService } from '../services/movimiento.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'

import { GlobalConfig } from  '../services/global-config'

declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'detalleMovimiento-add',
	templateUrl : '../views/detalleMovimiento-add.component.html',
	providers: [DetalleMovimientoService, MovimientoService]
})

export class DetalleMovimientoAdd{
	public dMov : DetalleMovimiento;
	public mov: Movimiento;
	public movimientos: Movimiento[];
	public id: number;


	constructor(
		private _detalleMovimientoService: DetalleMovimientoService,
		private _movimientoService: MovimientoService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.mov = new Movimiento(null,null,null,null,null, true);

		this.dMov = new DetalleMovimiento(null, null, null ,null, GlobalConfig.planillaId);


	}

	ngOnInit(){
		this.getMovimientos();
		this.id = GlobalConfig.planillaId;
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();

	}


	getMovimientos(){
		this._movimientoService.getMovimientos().subscribe(
			response =>{
				this.movimientos = response;
				console.log(this.movimientos);

			},
			error=>{
				console.log(<any>error);
			}

		);
	}


	onSubmit(){

		console.log(this.dMov);
		console.log(this.mov);
		this._detalleMovimientoService.addDetalleMovimiento(this.dMov).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡El tipo detalle de movimiento fue guardado exitosamente!'
	      			});
					this._router.navigate(['/planilla/' + GlobalConfig.planillaId]);
				},

				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'

	      		});
					this._router.navigate(['/planilla/' + GlobalConfig.planillaId]);
				}

			);

	}

	retroceder(){
		console.log("entra");
		this._router.navigate(['/planilla/' + GlobalConfig.planillaId]);
	}

}