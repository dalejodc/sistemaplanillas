import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { RolService } from '../services/rol.service';
import { MenuService } from '../services/menu.service';
import { Rol } from '../models/rol';
import { Menu } from '../models/menu';
import { Vista } from '../models/vista';

@Component({
	selector: 'rol-add',
	templateUrl: '../views/rol-add.component.html',
	providers: [RolService, MenuService]
})

export class RolAddComponent {

	public rol: Rol;
	public listaMenus: Menu;
	public asignacionRolMenu :Vista[] //Almacenará las vistas seleccionadas

	constructor(
		private _rolService: RolService,
		private _menuService: MenuService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.rol= new Rol ( null, null, "", "", true);
		this.asignacionRolMenu = [];
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getMenus();
	}

	getMenus(){
		this._menuService.getMenus().subscribe(
			result=>{
				this.listaMenus = result;
				console.log(this.listaMenus);
			},
			error=>{
				console.log(<any>error);
			}
			);
	}

	//Método para verificar la selección o deselección del box
	onCheckboxChange(id, event) {
		if(event.target.checked) { //si es seleccionado ingresa el objeto al arreglo con el id
		  this.asignacionRolMenu.push({"menu_id": id});
		} else {
		//De lo contrario, si es desmarcado, hay que recorrer el arreglo para quitar el 
		//elemento 
		  for (let x in this.asignacionRolMenu){
				let index = this.asignacionRolMenu.findIndex(x => x.menu_id == id);
				this.asignacionRolMenu.splice(index, 1)
		  	 }
		  }
		}

	guardarRol(){
		console.log(this.asignacionRolMenu);
		console.log(sessionStorage.getItem('token'));
		
		interface ROLL {
			    rol_nombre: string,
			    rol_descripcion : string,
			    rol_estado : true,
			    asignacionRolMenu : Vista[]
			};

		//Creando objeto auxiliar para que sea aceptado en el formato de la URL
		const ROLL : ROLL = {
			rol_nombre : this.rol.rol_nombre,  
			rol_descripcion : this.rol.rol_descripcion, 
			rol_estado: true, 
			asignacionRolMenu : this.asignacionRolMenu
		};

		console.log(ROLL);

		console.log(JSON.stringify(ROLL));
		this._rolService.addRol(ROLL).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡El rol ' + this.rol.rol_nombre+ ' fue guardado exitosamente!'
	      			});
				this._router.navigate(['/roles']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
			}
		);
	}
}