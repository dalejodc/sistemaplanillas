import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';
declare var $:any;
declare var Materialize:any;

import { EmpresaService } from '../services/empresa.service';
import { EstructuraTerritorialService } from '../services/estructura-territorial.service';
import { PlanService } from '../services/plan.service';
import { Empresa } from '../models/empresa';

@Component({
	selector: 'registro',
	templateUrl: '../views/ventajas.component.html',
	providers: [EmpresaService, EstructuraTerritorialService, PlanService]
})

export class RegistroComponent {

	public empresa: Empresa;
	public paises: number;
	public planes: number;

	constructor(
		private _empresaService: EmpresaService,
		private _paisService: EstructuraTerritorialService,
		private _planService: PlanService,
		private _router: Router,
		private alertService: SweetAlertService
	){
		this.empresa= new Empresa(null, 1, null, null, null, null,null,null, null, null);
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getPaises();
		this.getPlanes();
	}

	getPaises(){
		this._paisService.getPaises().subscribe(
			result => {
				this.paises = result;
				console.log(this.paises);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getPlanes(){
		this._planService.getPlanes().subscribe(
			result => {
				this.planes = result;
				console.log(this.planes);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	guardarEmpresa(){
		this.empresa.estado_id = 3;
		console.log(this.empresa);
		console.log(this.empresa.estado_id);

		this._empresaService.addEmpresa(this.empresa).subscribe(
			response =>{
				this.alertService.success({
	        			title: '¡La solicitud ' + this.empresa.empresa_nombre+ ' fue guardada exitosamente!'
	      			});
				this._router.navigate(['/']);	
			},
			error =>{
				this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				console.log(<any>error);
				console.log(error.data);
			}
		);
	}
}