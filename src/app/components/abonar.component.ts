import { Component } from '@angular/core'
import { Abono } from '../models/abono'
import { CentroDeCostos } from '../models/centro_de_costos'
import { CentroDeCostosService} from '../services/centro_de_costos.service'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { SweetAlertService } from 'angular-sweetalert-service'

import { GlobalConfig } from  '../services/global-config'

declare var $:any;
declare var Materialize:any;

@Component({
	selector : 'abono-add',
	templateUrl : '../views/abono-add.component.html',
	providers: [CentroDeCostosService]
})

export class AbonoAdd{
	public abono1 : Abono;
	public saladoActual: number;
	public centroCosto: CentroDeCostos;

	constructor(
		private _centroClService: CentroDeCostosService,
		private _route: ActivatedRoute,
		private _router: Router,
		private alertService: SweetAlertService
		){

		this.abono1 = new Abono(null, null);

	}

	ngOnInit(){
		this.getCentroCosto();
		$('.collapsible').collapsible();
		$(".button-collapse").sideNav();

	}


	getCentroCosto(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];


		this._centroClService.getCentroCosto(id).subscribe(
			response =>{
				this.centroCosto = response;
				console.log(this.centroCosto);
				
				this.saladoActual = this.centroCosto.centro_costo_presupuesto;
				this.abono1.centro_costo_id=this.centroCosto.centro_costo_id;
			

			},
			error =>{
				console.log(error);
			}
			);
	
	});

}

	onSubmit(){
		console.log(this.abono1);

		if (this.abono1.abono_presupuesto_monto>0){


		
			this._centroClService.addAbono(this.abono1).subscribe(
				response =>{

					this.alertService.success({
	        			title: '¡El abono de $ ' + this.abono1.abono_presupuesto_monto+ ' fue agregado exitosamente!'
	      			});
					this._router.navigate(['/centrocosto']);
				},

				error =>{
					console.log(error);
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      		});
				}

			);

	}

	
	else{

		this.alertService.error({
	        			title: '¡El monto es menor o igual a cero !'});

	}





}
	

}