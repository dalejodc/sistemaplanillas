import { Component } from '@angular/core'
import { SweetAlertService } from 'angular-sweetalert-service'

import { TipoTransaccion } from '../models/tipo_transaccion'
import { TipoTransaccionService } from '../services/tipo_transaccion.service'
declare var $:any;
declare var Materialize:any;

@Component({
	selector: 'tipoTransaccion',
	templateUrl: '../views/tipoTransaccion-list.component.html',
	providers: [TipoTransaccionService]
})

export class TipoTransaccionComponent{
	public listtiposTransacciones: TipoTransaccion[];

	constructor(
		private _tipoTransaccionService: TipoTransaccionService,
		private alertService: SweetAlertService	


		){

	}

	ngOnInit(){
		this.getTipoTransaccioes();
		$('.collapsible').collapsible();
 		$(".button-collapse").sideNav();

	}

	getTipoTransaccioes(){
		this._tipoTransaccionService.getTipoTransacciones().subscribe(
			response =>{
				this.listtiposTransacciones = response;
				console.log(this.listtiposTransacciones);

			},
			error =>{
				console.log(<any>error);
			}
			);
	}


	onDelete(id, transaccion: TipoTransaccion){
		

		this.alertService.confirm({
	      title: '¿Desea deshabilitar el tipo de Transacción: '+transaccion.tipo_transaccion_nombre+'?',
	      text: "La Transacción se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(()=> {
	    	this._tipoTransaccionService.deleteTipoTransaccion(id).subscribe(
				response =>{
					this.alertService.success({
					title: '¡El tipo Transacción ' + transaccion.tipo_transaccion_nombre + ' fue deshabilitado exitosamente!'
					});
					this.getTipoTransaccioes();

				},
				error => {
					this.alertService.error({
	        			title: '¡Ocurrió un error!',
	        			text:  transaccion.tipo_transaccion_nombre +", "+ error.statusText
	      			});
					console.log(<any>error)
					this.getTipoTransaccioes();
				}


			);

	    })
	    .catch(() => console.log('canceled'));		
	}
}