import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Subject } from 'rxjs/Rx';

declare var $:any;
declare var Materialize:any;

import { TipoDocumentoService } from '../services/tipo-documento.service';
import { TipoDocumento } from '../models/tipo_documento';

@Component({
	selector: 'tipo-contrato',
	templateUrl: '../views/tipo-documento-list.component.html',
	providers: [TipoDocumentoService]
})

export class TipoDocumentoComponent {

	public listaTiposDocumentos : TipoDocumento[];
	public tipoDocumento: TipoDocumento;

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _tipoDocumentoService: TipoDocumentoService,
		private alertService: SweetAlertService	
	){
		this.dtOptions = {
		    pageLength: 10,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getTiposDocumentos();
	}

	getTiposDocumentos(){
		this._tipoDocumentoService.getTiposDocumentos().subscribe(
			result => {
				this.listaTiposDocumentos = result;
				console.log(this.listaTiposDocumentos);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	// deshabilitar(tipoContrato){
	// 	this.alertService.confirm({
	//       title: '¿Desea deshabilitar el Tipo de Contrato '+tipoContrato.forma_contrato_nombre+'?',
	//       text: "El tipo de contrato se deshabilitará y no podrá asignarse a los empleados.",
	//       cancelButtonColor: '#E57373',
 //  		  confirmButtonColor: '#83C283',
 //  		  confirmButtonText: 'Aceptar'
	//     })
	//     .then(() => {
	// 	    this.tipoContrato = tipoContrato;
	// 		this.tipoContrato.forma_contrato_estado =false;

	// 		this._tipoContratoService.deleteTipoContrato(tipoContrato.forma_contrato_id).subscribe(
	// 			response =>{
	// 				this.alertService.success({
	//         			title: '¡El Tipo de Contrato ' + this.tipoContrato.forma_contrato_nombre+ ' fue deshabilitado exitosamente!'
	//       			});
	// 				this.ngOnInit();
	// 			},
	// 			error =>{
	// 				this.alertService.error({
	//         			title: '¡Ocurrió un error!'
	//       			});
	// 				console.log(<any>error);
	// 			}
	// 		);	
	//     })
	//     .catch(() => console.log('canceled'));
	// }
}