import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

declare var $:any;
declare var Materialize:any;

import { ConfiguracionEmpresa } from '../models/configuracion_empresa';
import { InfoEmpresaService } from '../services/info-empresa.service';

@Component({
	selector: 'home',
	templateUrl: '../views/home.component.html',
	providers: [InfoEmpresaService]
})

export class HomeComponent {

	public rol : string;
	public usuario : string;
	public empresa : ConfiguracionEmpresa;
	public mostrar :boolean;
	public cantidad_empleados: string;

	constructor(
		private alertService: SweetAlertService,
		private _infoEmpresaService: InfoEmpresaService
	){
		this.empresa = new ConfiguracionEmpresa (null, null, null, null, null, null, null, null, null, null, null, null, null);
		this.mostrar= false;
	 }
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav()
		
		this.rol = sessionStorage.getItem('rol');
		this.usuario = sessionStorage.getItem('user');
		this.cantidad_empleados = sessionStorage.getItem('cant_empleados');
		console.log(sessionStorage.getItem('cant_empleados'));
		this.getEmpresa();


		// this.alertService.success({
		//   type: 'success',
  // 		  title: '¡Ha iniciado sessión como ' +sessionStorage.getItem('rol')+'!',
		//   toast : true,
		//   position: 'top-end',
		//   showConfirmButton: false,
		//   timer: 1000
		// });
	}

	getEmpresa(){
		this._infoEmpresaService.getEmpresa().subscribe(
			result => {
				this.empresa = result;
				console.log(this.empresa);

				sessionStorage.setItem('max_empleados', this.empresa.plan.plan_max_empleados.toString());

				if(this.empresa.empresa_nombre != null){
					console.log("no es null");
					this.mostrar = true;
				}
			},
			error => {
				console.log(<any>error);
			}
			);
	}

// 	mensaje(){
// 		const toast = this.alertService.;
// // 		const toast = swal.mixin({
// //   toast: true,
// //   position: 'top-end',
// //   showConfirmButton: false,
// //   timer: 3000
// // });

// 	toast({
// 	  type: 'success',
// 	  title: 'Signed in successfully'
// 	})
// 	}
}