import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { SweetAlertService } from 'angular-sweetalert-service';


declare var $:any;
declare var Materialize:any;

import { GeneroService } from '../services/genero.service';
import { Genero } from '../models/genero';

@Component({
	selector: 'genero',
	templateUrl: '../views/genero-list.component.html',
	providers: [GeneroService]
})

export class GeneroComponent implements OnInit {

	public listaGeneros : Genero[];
	public listaGenerosDeshabilitados : Genero[];
	public genero: Genero;

	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private _generoService: GeneroService,
		private alertService: SweetAlertService	
	){

		 // opciones de DataTable
		  this.dtOptions = {
		    pageLength: 10,
		    pagingType: 'simple_numbers',
		    order: [[0, "asc"]],
		    language: {
		      "emptyTable": "No hay registros en la tabla",
		      "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		      "infoEmpty": "",
		      "infoFiltered": "(filtrados de _MAX_ totales )",
		      "lengthMenu": " ",
		      "search": "Buscar:",
		      "zeroRecords": "Búsqueda sin resultados",
		      "paginate": {
		        "first": "Primero",
		        "last": "Último",
		        "next": "Siguiente",
		        "previous": "Anterior"
		      }
		    }
		  };
	}
	
	ngOnInit(){
		setTimeout(function(){$('.collapsible').collapsible()},2000);
		$(".button-collapse").sideNav();
		this.getGeneros();
	}

	getGeneros(){
		this._generoService.getGeneros().subscribe(
			result => {
				this.listaGeneros = result;
				console.log(this.listaGeneros);
				this.dtTrigger.next();
			},
			error => {
				console.log(<any>error);
			}
			);
	}
	// getGenerosDeshabilitados(){
	// 	this._generoService.getGenerosDeshabilitados().subscribe(
	// 		result => {
	// 			this.listaGenerosDeshabilitados = result;
	// 			console.log(this.listaGenerosDeshabilitados);
	// 		},
	// 		error => {
	// 			console.log(<any>error);
	// 		}
	// 		);
	// }

	deshabilitar(genero){
		this.alertService.confirm({
	      title: '¿Desea deshabilitar el género '+genero.genero_nombre+'?',
	      text: "El género se deshabilitará y no podrá asignarse a los empleados.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
		    this.genero = genero;
			this.genero.genero_estado =false;

			this._generoService.editGenero(genero.genero_id, this.genero).subscribe(
				response =>{
					this.alertService.success({
	        			title: '¡El género ' + this.genero.genero_nombre+ ' fue deshabilitado exitosamente!'
	      			});
					this.ngOnInit();
				},
				error =>{
					this.alertService.error({
	        			title: '¡Ocurrió un error!'
	      			});
					console.log(<any>error);
				}
			);	
	    })
	    .catch(() => console.log('canceled'));
	}
}