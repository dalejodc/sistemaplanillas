import { Movimiento } from './movimiento'

export class DetalleMovimiento {
	
	constructor(
		public detalle_movimiento_id: number,
		public detalle_movimiento_monto: number,
		public movimiento_id: number,
		public movimiento: Movimiento,
		public planilla_id: number
		) {
	
	}
}