import { EstadoCivil } from './estado_civil'
import { Genero } from './genero'
import { Profesion } from './profesion'
import { Empleado } from './empleado'
import { Correo } from './correo'
import { Direccion } from './direccion'


import { Documento } from './documento'

import { Telefono } from './telefono'

export class Persona{
	constructor(
		public persona_id: number,
		public persona_direccion_id: number,
		public direccion: Direccion,
		public estado_civil_id: number,
		public persona_estado_civil: EstadoCivil,
		public genero_id: number,
		public persona_genero: Genero,
		public profesion_id: number,
		public persona_profesion: Profesion,
		public persona_primer_nombre: string,
		public persona_segundo_nombre: string,
		public persona_primer_apellido: string,
		public persona_segundo_apellido:string,
		public persona_fecha_nacimiento: Date,

		public documentos: Documento[],
		public correos: Correo[],
		public telefonos: Telefono[]


		){}
}