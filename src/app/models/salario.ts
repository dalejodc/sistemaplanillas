import {TipoSalario } from './tipo_salario'

export class Salario{
	constructor(
		public salario_id:number,
		public tipo_salario_id: number,
		public tipo_salario:TipoSalario,
		public empleado_id: number,
		public salario_monto_base: number
		){}
}