export class TipoDocumento{

	constructor(
		public tipo_documento_id: number,
		public empresa_id: number,
		public tipo_documento_nombre: string,
		public tipo_documento_num_caracter: number,
		public es_obligacion: boolean
	){}
}
