export class Menu{
	constructor(
		public menu_id: number,
		public menu_nombre: string,
		public menu_url: string,
		public menu_icono : string
	){}
}