import {NivelEstructura} from './nivel_estructura'

export class Puesto{
	
	constructor(
		public puesto_id : number,

	  	public nivel_estructura_id: number,
	  	public nivel_estructura: NivelEstructura,
		
		public puesto_nombre : string,
		public puesto_descripcion : string,
		public puesto_estado : boolean,
		public puesto_tiene_comision : boolean


		) {}
}
