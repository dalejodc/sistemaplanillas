import { DetalleMovimiento } from './detalleMovimiento'
import { PeriodoPlanilla } from './periodoPlanilla'
import { Empleado } from './empleado'

export class Planilla{
	constructor(
		public planilla_id: number,
		public planilla_fecha_generacion: Date,
		public planilla_total:number,
		public detalleMovimiento: DetalleMovimiento[],
		public periodoPlanilla: PeriodoPlanilla,
		public empleado_id: number,
		public empleado: Empleado,
		public planilla_comision: number,
		public planilla_venta: number,
		public planilla_renta: number
		){}
}