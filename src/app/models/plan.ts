export class Plan{

	constructor(
		//public plan_id: number,
		public plan_nombre: string,
		public plan_descripcion: string,
		public plan_precio : number,
		public plan_max_empleados : number
	){}
}
