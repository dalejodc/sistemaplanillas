import { EstructuraTerritorial } from './estructura_territorial'
export class Direccion{

	constructor(
		public direccion_id: number,
		public estructura_territorial_id: number,
		public estructura_territorial: EstructuraTerritorial,
		public direccion_nombre: string



		){

	}
}

