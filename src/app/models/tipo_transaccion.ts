export class TipoTransaccion{

	constructor(
		public tipo_transaccion_id: number,
		public tipo_transaccion_nombre: string,
		public tipo_transaccion_descripcion: string,
		public tipo_transaccion_obligatorio: boolean,
		public tipo_transaccion_signo: string

		){

	}

}