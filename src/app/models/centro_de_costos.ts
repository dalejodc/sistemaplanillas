import { NivelEstructura} from './nivel_estructura'
import { Abono} from './abono'
export class CentroDeCostos{
	constructor(
		public centro_costo_id: number,
		public centro_costo_monto: number,
		public centro_costo_presupuesto: number,
		public nivelEstructura: NivelEstructura,
		public abonoPresupuesto: Abono[]

		){}
}