export class Genero{

	constructor(
		public genero_id: number,
		public genero_nombre: string,
		public genero_descripcion: string,
		public genero_estado : boolean
	){}
}
