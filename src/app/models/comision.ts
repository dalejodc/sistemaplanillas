export class Comision{
	constructor(
		public comision_id: number,
		public comision_minimo: number,
		public comision_maximo: number,
		public comision_porcentaje: number,
		public comision_nombre: string


		){}
}