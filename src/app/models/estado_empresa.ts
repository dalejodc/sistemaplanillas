export class Estado{

	constructor(
		public estado_id: number,
		public estado_nombre: string,
		public estado_descripcion: string,
	){}
}
