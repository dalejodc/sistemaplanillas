
export class Renta{

	constructor(
		public renta_id: number,
		public periodo_id: number,
		public renta_nombre: string,
		public renta_descripcion: string,
		public renta_minimo : number,
		public renta_maximo : number,
		public renta_porcentaje : number,
	){}
}
