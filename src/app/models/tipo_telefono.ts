export class TipoTelefono{

	constructor(
		public tipo_telefono_id: number,
		public tipo_telefono_nombre: string,
		public tipo_telefono_descripcion: string,
	){}
}
