import { Persona } from './persona'
import { Puesto } from './puesto'
import { DetalleContrato } from './detalleContrato'
import { Salario } from './salario'

export class Empleado{

	constructor(
		public empleado_id: number,
		public empleado_persona_id: number,
		public persona: Persona,
		public empleado_usuario_id: number,//no va en el formulario
		public empleado_detalle_contrato_id: number,
		public detalle_contrato: DetalleContrato,
		public puesto_id: number,
		public puesto: Puesto,
		public emp_empleado_id: number,//empresa id se le agrega en el backend
		public empleado_salario_id: number,
		public salario: Salario,
		public empleado_codigo: string,//Se genera en la base
		public empleado_fecha_ingreso: Date,//no va en el formulario
		public empleado_fecha_retiro:Date,//no van en el formulario
		public empleado_estado: boolean

		){}
}  