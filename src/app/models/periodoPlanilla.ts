import {Planilla} from './planilla'
export class PeriodoPlanilla{
	constructor(
		public periodo_planilla_id: number,
		public empresa_id: number,
		public periodo_planilla_fecha_inicio: Date,
		public periodo_planilla_fecha_fin: Date,
		public planilla: Planilla[]
	)
	{}
}
