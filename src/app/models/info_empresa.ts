export class InfoEmpresa{

	constructor(
		public empresa_id: number,
		public empresa_nombre: string,
		public empresa_direccion: string,
		public empresa_nit: string,
		public empresa_nic: string,
		public empresa_pagina_web: string,
		public empresa_logo : string
	){}
}

  