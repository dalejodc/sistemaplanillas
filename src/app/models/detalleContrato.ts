import { TipoContrato } from './tipo_contrato'
export class DetalleContrato{
	constructor(
		public detalle_contrato_id: number,
		public empleado_id: number,
		public contrato_id: number,
		public contrato: TipoContrato,
		public detalle_contrato_fecha_inicio: Date,
		public detalle_contrato_fecha_fin: Date,

		){}
}