export class EstructuraTerritorial{

	constructor(
		public estructura_territorial_id: number,
		public est_estructura_territorial_id: number,
		public estructura_territorial_nombre: string,
		public estructura_territorial : EstructuraTerritorial
	){}
}