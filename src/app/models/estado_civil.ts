export class EstadoCivil{

	constructor(
		public estado_civil_id: number,
		public estado_civil_nombre: string,
		public estado_civil_descripcion: string,
		public estado_civil_estado : boolean
	){}
}