export class TipoContrato{

	constructor(
		public forma_contrato_id: number,
		public forma_contrato_nombre: string,
		public forma_contrato_descripcion: string,
		public forma_contrato_estado : boolean
	){}
}


/*
forma_contrato_id":1,
"forma_contrato_nombre":"Contrato 1",
"forma_contrato_tipo":"Tipo de contrato",
"forma_contrato_descripcion":"descripci\u00f3n",
"forma_contrato_estado":true}
*/
/*

forma_contrato_id":1,
"forma_contrato_nombre":"Individual",
"forma_contrato_descripcion":"Contrato Individual,
"forma_contrato_estado":true
*/