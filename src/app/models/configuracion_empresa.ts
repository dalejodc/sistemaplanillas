import { Plan } from './plan'
import { Estado } from './estado_empresa'
import { EstructuraTerritorial } from './estructura_territorial'

export class ConfiguracionEmpresa{

	constructor(
		public empresa_id: number,
		public estado_id: number,
		public estado : Estado,
		public plan_id: number,
		public plan : Plan,
		public estructura_territorial_id: number,
		public estructura_territorial: EstructuraTerritorial,
		public empresa_nombre: string,
		public empresa_direccion: string,
		public empresa_representante: string,
		public empresa_pagina_web: string,
		public empresa_logo: string,
		public empresa_correo_representante : string
	){}
}

/* {
  "empresa_id": 2,
  "estado": {
    "estado_id": 1,
    "estado_nombre": "Activa",
    "estado_descripcion": "Empresa activa"
  },
  "documentoEmpresa": [],
  "empresa_nombre": "Escuela Mejicanos",
  "empresa_direccion": "Frente a Gasolinera, edificio 4",
  "empresa_representante": "Juan José",
  "empresa_pagina_web": "escuela.com",
  "empresa_fecha_solicitud": "2018-05-24 00:00:00",
  "empresa_fecha_aprobacion": "2018-05-24 00:00:00",
  "empresa_correo_representante": "mm14089@ues.edu.sv",
  "empresa_logo": "https://res.cloudinary.com/keystone-demo/image/upload/v1525583104/kevpscahffatjixzkuyj.jpg",
  "plan_id": 3,
  "plan": {
    "plan_id": 3,
    "plan_nombre": "PREMIUM",
    "plan_descripcion": "SIPEL para grandes negocios",
    "plan_precio": "4000.00",
    "plan_max_empleados": 10000
  },
  "estructura_territorial_id": 1,
  "estructuraTerritorial": {
    "estructura_territorial_id": 1,
    "est_estructura_territorial_id": null,
    "tipo_estructura_id": 1,
    "estructura_territorial_nombre": "El salvador"
  }
} */