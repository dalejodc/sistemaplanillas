import { TipoTransaccion } from './tipo_transaccion'

export class Movimiento{

	constructor(
		public movimiento_id: number,
		public tipo_transaccion_id: number,
		public tipoTransaccion: TipoTransaccion,
		public movimiento_nombre: string,
		public movimiento_descripcion: string,
		public es_obligacion: boolean


		){}
}