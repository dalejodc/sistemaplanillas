import { Puesto } from './puesto'
//import { Empresa } from './empresa'
//import { ConfiguracionEmpresa } from './configuracion_empresa';
export class CategoriaSalario{
	
	constructor(
		public categoria_salario_id : number,
		
		public puesto_id : number,
		public puesto : Puesto,
		
		//public empresa_id : number,
		//public empresa : Empresa,
		
		public categoria_salario_nombre : string,
		public categoria_salario_descripcion : string,
		public categoria_salario_minimo : number,
		public categoria_salario_maximo : number,
		public categoria_salario_estado : boolean

		) {}
}