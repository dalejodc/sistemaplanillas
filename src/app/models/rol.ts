export class Rol{
	constructor(
		public rol_id: number,
		public empresa_id: number,
		public rol_nombre: string,
		public rol_descripcion: string,
		public rol_estado : boolean
	){}
}
