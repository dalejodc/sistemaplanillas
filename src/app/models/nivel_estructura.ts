import {TipoNivel} from './tipo_nivel'

export class NivelEstructura{

	constructor(
		public nivel_estructura_id: number,
		public niv_nivel_estructura_id: number,
		public nivelEstructura: NivelEstructura[],
		public centro_de_costo_id: number,
		public tipo_nivel_id: number,
		public nivel_estructura_nombre: string,
		public tipo_nivel : TipoNivel
	){}
}

// {
// 	"nivel_estructura_id":#,
// 	"niv_nivel_estructura_id":#,
// 	"centro_costo_id":#,
// 	"tipo_nivel_id":#,"
// 	nivel_estructura_nombre":" ",
// 	"tipoNivel":{
// 		"tipo_nivel_nombre":" ",
// 		"tipo_nivel_id":#
// 	}
// }