export class Empresa{

	constructor(
		public empresa_id: number,
		public estado_id: number,
		public plan_id: number,
		public estructura_territorial_id: number,
		public empresa_nombre: string,
		public empresa_direccion: string,
		public empresa_representante: string,
		public empresa_pagina_web: string,
		public empresa_logo: string,
		public empresa_correo_representante : string
	){}
}