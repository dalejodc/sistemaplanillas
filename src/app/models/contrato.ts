export class Contrato{
	constructor(
		public contrato_id: number,
		public forma_contrato_id: number,
		public contrato_descripcion: string,
		public contrato_duracion: number,
		public contrato_estado : boolean
	){}
}

