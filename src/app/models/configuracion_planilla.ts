import { Empresa } from './empresa'
import { Periodo } from './periodo'

export class ConfiguracionPlanilla{

	constructor(
		public configuracion_planilla_id: number,
		
		public empresa_id : number,
		public empresa : Empresa,

		public periodo_id: number,
		public periodo : Periodo,

		public configuracion_planilla_min_sal: number	
		
	){}
}