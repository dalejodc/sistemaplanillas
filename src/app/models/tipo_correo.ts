export class TipoCorreo{

	constructor(
		public tipo_correo_id: number,
		public tipo_correo_nombre: string,
		public tipo_correo_descripcion: string,
	){}
}
